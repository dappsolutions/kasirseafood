import 'package:flutter/material.dart';

class TentangView extends StatelessWidget {
  Color hexToColor(String code) {
    return new Color(int.parse(code.substring(1, 7), radix: 16) + 0xFF000000);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.blue,
      body: Container(
        margin: EdgeInsets.only(left: 32),
        child: Column(
          children: <Widget>[
            Container(
                alignment: Alignment.centerLeft,
                margin: EdgeInsets.only(top: 270),
                child: Text("Tentang", style: TextStyle(fontSize : 40,color: Colors.white),)
            ),
            Container(
                alignment: Alignment.centerLeft,
                child: Text("Aplikasi", style: TextStyle(fontSize : 40, color: Colors.white),)
            ),
            Container(
                alignment: Alignment.centerLeft,
                child: Text("DAPPSPOS", style: TextStyle(fontSize : 40, color: Colors.white),)
            ),
            Container(
              alignment: Alignment.centerLeft,
              child: Text("Aplikasi Kasir untuk warung seafood\n", style: TextStyle(color: Colors.white),),
            )
          ],
        ),
      ),
    );
  }
}
