import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:footer/footer.dart';
import 'package:footer/footer_view.dart';
import 'package:get/get.dart';
import 'package:search_page/search_page.dart';
import 'package:wponline/app/controllers/tipeikan.controllers.dart';
import 'package:wponline/app/models/produk.models.dart';
import 'package:wponline/app/models/tipeikan.models.dart';
import 'package:wponline/app/storage/produk.dart';
import 'package:wponline/app/storage/produk_has_stok.dart';
import 'package:wponline/resources/views/dashboard/footer.views.dart';
import 'package:wponline/resources/views/dashboard/title.views.dart';
import 'package:wponline/resources/views/kasir/titleform.kasir.views.dart';
import 'package:wponline/resources/views/tipeikan/formadd.tipeikan.views.dart';
import 'package:wponline/resources/views/tipeikan/formstok.tipeikan.views.dart';


class MainTipeIkanView extends StatefulWidget {
  @override
  _MainTipeIkanViewState createState() => _MainTipeIkanViewState();
}

class _MainTipeIkanViewState extends State<MainTipeIkanView> with WidgetsBindingObserver {
  TipeIkanController controllers;

  @override
  void initState() {
    controllers = new TipeIkanController();
//    controllers.getListProduk();
    controllers.getListStok();
    super.initState();
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: FooterView(
        children: <Widget>[
          TitleView(title: "DAPPSPOS",),
          TitleFormView(title: "DAFTAR JENIS & STOK IKAN",),
          Container(
            padding: EdgeInsets.only(left: 32, right: 32, top: 8),
            width: double.infinity,
            height: 70,
            child: RaisedButton(
              child: Text("TAMBAH", style: TextStyle(color: Colors.white),),
              color: Colors.pinkAccent,
              onPressed: (){
                  Get.to(FormAddTipeIkanView(controller: controllers,));
              },
              elevation: 8,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(8.0),
              ),
            ),

          ),
          Container(
            padding: EdgeInsets.only(left: 16, right: 16),
            child: GetBuilder<TipeIkanController>(
                init: controllers,
                builder: (params){
                  if(!params.reload){
                    return ListView.builder(
                      itemCount: ProdukModel.data_stok.length,
                      shrinkWrap: true,
                      itemBuilder: (context, index) {
                        final Produk_has_stok p_trans = ProdukModel.data_stok[index];
                        return ListTile(
                          title: Text(p_trans.jenis, style: TextStyle(fontWeight: FontWeight.bold),),
                          subtitle: Text('HARGA : Rp, ${p_trans.harga} \nSTOK : ${p_trans.stok_outstanding} Kg'),
                          trailing: GestureDetector(
                            child: Icon(Icons.delete, color: Colors.pinkAccent,),
                            onTap: () async{
                              controllers.deleteData(p_trans.id);
                            },
                          ),
                          onTap: () async{
                            ProdukModel.jenis_produk = p_trans.jenis;
                            ProdukModel.stok_awal = p_trans.stok_awal;
                            ProdukModel.jumlah_stok = "0";
                            ProdukModel.stok_oustanding = p_trans.stok_outstanding;
                            String result = await Get.to(FormAddStokIkanView(
                              id_ikan: p_trans.id,
                              controller: controllers,
                            ));

//                            print("RESULT ${result}");
                            if(result == "reload"){
                                controllers.reload = true;
                                controllers.update();
                                controllers.getListStok();
                            }
                          },
                        );
                      },
                    );
                  }

                  return CircularProgressIndicator();
                }
            )
          )
        ],
        footer: Footer(
          backgroundColor: Colors.white,
          child: FooterDashboardView(),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.search),
        backgroundColor: Colors.pinkAccent,
        tooltip: "Cari Jenis",
        onPressed: () => showSearch(
          context: context,
          delegate: SearchPage<Produk_has_stok>(
            items: ProdukModel.data_stok,
            searchLabel: 'Cari Jenis',
            suggestion: Center(
              child: Text('Cari Antrian Berdasarkan Jenis'),
            ),
            failure: Center(
              child: Text('Tidak ada antrian ditemukan :('),
            ),
            filter: (data) => [
              data.jenis,
            ],
            builder: (data) => ListTile(
              title: Text(data.jenis, style: TextStyle(fontWeight: FontWeight.bold),),
              subtitle: Text('STOK : ${data.stok_outstanding} Kg'),
              trailing: Text('MENU', style: TextStyle(color: Colors.pinkAccent),),
              onTap: () async{
                ProdukModel.jenis_produk = data.jenis;
                ProdukModel.stok_awal = data.stok_awal;
                ProdukModel.jumlah_stok = "0";
                ProdukModel.stok_oustanding = data.stok_outstanding;
                String result = await Get.to(FormAddStokIkanView(
                  id_ikan: data.id,
                  controller: controllers,
                ));

//                            print("RESULT ${result}");
                if(result == "reload"){
                  controllers.reload = true;
                  controllers.update();
                  controllers.getListStok();
                }
              },
            ),
          ),
        ),
      ),
    );
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    if(state == AppLifecycleState.inactive){
      print("IN AKTIF");
    }

    if(state == AppLifecycleState.paused){
      print("IN PAUSED");
    }

    if(state == AppLifecycleState.resumed){
      print("IN RESUME BRO");
    }
  }
}
