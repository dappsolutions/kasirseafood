import 'package:flutter/material.dart';
import 'package:footer/footer.dart';
import 'package:footer/footer_view.dart';
import 'package:get/get.dart';
import 'package:wponline/app/controllers/tipeikan.controllers.dart';
import 'package:wponline/app/models/produk.models.dart';
import 'package:wponline/resources/views/dashboard/footer.views.dart';
import 'package:wponline/resources/views/dashboard/title.views.dart';
import 'package:wponline/resources/views/kasir/titleform.kasir.views.dart';

class FormAddStokIkanView extends StatefulWidget {
  String id_ikan = "";
  TipeIkanController controller;
  FormAddStokIkanView({this.id_ikan, this.controller});

  @override
  _FormAddStokIkanViewState createState() => _FormAddStokIkanViewState();
}

class _FormAddStokIkanViewState extends State<FormAddStokIkanView> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: FooterView(
          children: <Widget>[
            TitleView(title: "DAPPSPOS",),
            TitleFormView(title: "TAMBAH STOK IKAN",),
            Container(
                padding: EdgeInsets.only(left: 32, right: 32, top: 16),
                child: TextFormField(
                  decoration: InputDecoration(
                      prefixIcon: Icon(Icons.confirmation_number),
                      border: OutlineInputBorder(),
                      hintText: "Jenis Ikan",
                      labelText: "Jenis Ikan "
                  ),
                  keyboardType: TextInputType.text,
                  initialValue: ProdukModel.jenis_produk,
                  enabled: false,
                  validator: (text){
                    if(text.isEmpty){
                      return "Jenis Ikan Wajib Diisi";
                    }
                    return null;
                  },
                  onChanged: (val){
                    ProdukModel.jenis_produk = val.toString();
                  },
                )
            ),
            Container(
                padding: EdgeInsets.only(left: 32, right: 32, top: 16),
                child: TextFormField(
                  decoration: InputDecoration(
                      prefixIcon: Icon(Icons.confirmation_number),
                      border: OutlineInputBorder(),
                      hintText: "Sisa Stok",
                      labelText: "Sisa Stok "
                  ),
                  keyboardType: TextInputType.number,
                  initialValue: ProdukModel.stok_oustanding,
                  enabled: false,
                  validator: (text){
                    if(text.isEmpty){
                      return "Sisa Stok Wajib Diisi";
                    }
                    return null;
                  },
                  onChanged: (val){

                  },
                )
            ),
            Container(
                padding: EdgeInsets.only(left: 32, right: 32, top: 16),
                child: TextFormField(
                  decoration: InputDecoration(
                      prefixIcon: Icon(Icons.confirmation_number),
                      hintText: " Tambah Stok",
                      labelText: " Tambah Stok "
                  ),
                  keyboardType: TextInputType.number,
                  initialValue: ProdukModel.jumlah_stok,
                  validator: (text){
                    if(text.isEmpty){
                      return "Jumlah Stok Wajib Diisi";
                    }
                    return null;
                  },
                  onChanged: (val){
                    ProdukModel.jumlah_stok = val.toString();
                  },
                )
            ),
            Container(
                padding: EdgeInsets.only(left: 32, right: 32, top: 16),
                child: TextFormField(
                  decoration: InputDecoration(
                      prefixIcon: Icon(Icons.people),
                      hintText: " Supplier",
                      labelText: " Supplier "
                  ),
                  keyboardType: TextInputType.text,
                  initialValue: ProdukModel.supplier,
                  validator: (text){
                    if(text.isEmpty){
                      return "Supplier Wajib Diisi";
                    }
                    return null;
                  },
                  onChanged: (val){
                    ProdukModel.supplier = val.toString();
                  },
                )
            ),
            Container(
              padding: EdgeInsets.only(left: 32, right: 32, top: 16),
              width: double.infinity,
              height: 70,
              child: RaisedButton(
                child: Text("PROSES", style: TextStyle(color: Colors.white),),
                color: Colors.green,
                onPressed: () async{
                  String message = await widget.controller.simpanStok(widget.id_ikan);
                  if(message == "success"){
                      Get.back(result: "reload");
                  }
                },
                elevation: 8,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(8.0),
                ),
              ),
            ),
          ],
          footer: Footer(
            backgroundColor: Colors.white,
            child: FooterDashboardView(),
          ),
        )
    );
  }
}
