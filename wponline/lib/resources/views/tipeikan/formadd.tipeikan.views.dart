import 'package:flutter/material.dart';
import 'package:footer/footer.dart';
import 'package:footer/footer_view.dart';
import 'package:get/get.dart';
import 'package:wponline/app/controllers/tipeikan.controllers.dart';
import 'package:wponline/app/models/produk.models.dart';
import 'package:wponline/resources/views/dashboard/footer.views.dart';
import 'package:wponline/resources/views/dashboard/title.views.dart';
import 'package:wponline/resources/views/kasir/titleform.kasir.views.dart';

class FormAddTipeIkanView extends StatefulWidget {
  TipeIkanController controller;
  FormAddTipeIkanView({this.controller});

  @override
  _FormAddTipeIkanViewState createState() => _FormAddTipeIkanViewState();
}

class _FormAddTipeIkanViewState extends State<FormAddTipeIkanView> {

  @override
  void initState() {
    super.initState();
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: FooterView(
        children: <Widget>[
          TitleView(title: "DAPPSPOS",),
          TitleFormView(title: "TAMBAH JENIS IKAN",),
          Container(
              padding: EdgeInsets.only(left: 32, right: 32, top: 16),
              child: TextFormField(
                decoration: InputDecoration(
                    prefixIcon: Icon(Icons.confirmation_number),
                    hintText: "Jenis Ikan",
                    labelText: "Jenis Ikan "
                ),
                keyboardType: TextInputType.text,
                validator: (text){
                  if(text.isEmpty){
                    return "Jenis Ikan Wajib Diisi";
                  }
                  return null;
                },
                onChanged: (val){
                    ProdukModel.jenis_produk = val.toString();
                },
              )
          ),
          Container(
              padding: EdgeInsets.only(left: 32, right: 32, top: 16),
              child: TextFormField(
                decoration: InputDecoration(
                    prefixIcon: Icon(Icons.attach_money),
                    hintText: "Harga Ikan",
                    labelText: "Harga Ikan "
                ),
                keyboardType: TextInputType.number,
                validator: (text){
                  if(text.isEmpty){
                    return "Harga Ikan Wajib Diisi";
                  }
                  return null;
                },
                onChanged: (val){
                    ProdukModel.harga_produk = val.toString();
                },
              )
          ),
          Container(
            padding: EdgeInsets.only(left: 32, right: 32, top: 16),
            width: double.infinity,
            height: 70,
            child: RaisedButton(
              child: Text("PROSES", style: TextStyle(color: Colors.white),),
              color: Colors.green,
              onPressed: () async{
                  Get.back();
                  widget.controller.simpan();
//                Get.snackbar("Informasi", "Berhasil disimpan");
//                Get.back();
//                Navigator.pop(context);
              },
              elevation: 8,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(8.0),
              ),
            ),

          ),
        ],
        footer: Footer(
          backgroundColor: Colors.white,
          child: FooterDashboardView(),
        ),
      )
    );
  }
}
