
import 'dart:io';

import 'package:blue_thermal_printer/blue_thermal_printer.dart';
import 'package:esc_pos_printer/esc_pos_printer.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:footer/footer.dart';
import 'package:footer/footer_view.dart';
import 'package:get/get.dart';
import 'package:path_provider/path_provider.dart';
import 'package:wponline/app/controllers/kasir.controllers.dart';
import 'package:wponline/app/controllers/printdapur.controllers.dart';
import 'package:wponline/app/controllers/transaksi.controllers.dart';
import 'package:wponline/app/models/kasir.models.dart';
import 'package:wponline/app/models/transaksi.models.dart';
import 'package:wponline/resources/views/dashboard/footer.views.dart';
import 'package:wponline/resources/views/dashboard/title.views.dart';
import 'package:wponline/resources/views/kasir/titleform.kasir.views.dart';
import 'package:wponline/resources/views/printing/main.printing.views.dart';
import 'package:wponline/resources/views/printing/printing.dapur.views.dart';

class DetailPesananKasirView extends StatefulWidget {
  bool addingMenu = false;
  DetailPesananKasirView({this.addingMenu});
  @override
  _DetailPesananKasirViewState createState() => _DetailPesananKasirViewState();
}

class _DetailPesananKasirViewState extends State<DetailPesananKasirView> {
  PrinterBluetoothManager printerManager = PrinterBluetoothManager();
  TransaksiController controller;
  PrintDapurController controller_print;

  @override
  void initState() {
    controller = TransaksiController();
    controller.updateReloadTrans(true);
    controller.getListPesanan(widget.addingMenu);

    controller_print = new PrintDapurController();
    controller_print.cariBluetoothPrinter(printerManager);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        body: FooterView(
          children: <Widget>[
            TitleView(title: "KASIR DAPPSPOS",),

            !widget.addingMenu ? TitleFormView(title: "${KasirModel.no_nota} \nANTRIAN : ${KasirModel.no_antrian}") :
            TitleFormView(title: "${KasirModel.no_nota}\nTAMBAHAN : ${KasirModel.no_tambahan.replaceAll(KasirModel.no_nota, "")} \nANTRIAN : ${KasirModel.no_antrian}") ,
            GetBuilder<PrintDapurController>(
              init: controller_print,
              builder: (params){
                if(!params.scanning){
                  if(params.devices.length > 0){
                    return TitleFormView(title: "PRINTER : ${params.devices[0].address}");
                  }
                }

                return TitleFormView(title: "PRINTER : -");
              },
            ),
            Container(
              padding: EdgeInsets.only(left: 32, right: 32, top: 16),
//            height: 80,
              child: Column(
                children: <Widget>[
                  Container(
                    child: Row(
                      children: <Widget>[
                        Text("Jml. Org", style: TextStyle(fontWeight: FontWeight.bold),),
                        Spacer(),
                        Text("Jml. Ikan", style: TextStyle(fontWeight: FontWeight.bold)),
                        Spacer(),
                        Text("Berat. Ikan (Kg)", style: TextStyle(fontWeight: FontWeight.bold)),
                        Spacer(),
                        Text("Jenis. Ikan", style: TextStyle(fontWeight: FontWeight.bold)),
                      ],
                    ),
                  ),
                  Padding(
                    padding:EdgeInsets.symmetric(horizontal:6.0),
                    child:Container(
                      height:1.0,
                      width:double.infinity,
                      color:Colors.black,),),
                  Container(
                      child: GetBuilder<TransaksiController>(
                          init: controller,
                          builder: (params){
                            if(!TransaksiModels.reload){
                              return ListView.builder(
                                  itemCount: TransaksiModels.data.length,
                                  shrinkWrap: true,
                                  itemBuilder: (context, item){
                                    return  Column(
                                      children: <Widget>[
                                        Container(
                                          child: Row(
                                            children: <Widget>[
                                              item == 0 ? Text("${TransaksiModels.data[item].qty_org}") : Text("-"),
                                              Spacer(),
                                              Text("${TransaksiModels.data[item].qty_produk}"),
                                              Spacer(),
                                              Text("${TransaksiModels.data[item].berat_produk}"),
                                              Spacer(),
                                              Text("${TransaksiModels.data[item].jenis_produk}"),
                                            ],
                                          ),
                                        ),
                                        SizedBox(height: 6,),

                                        Container(
                                          alignment: Alignment.centerRight,
                                          child: Text("${TransaksiModels.data[item].keterangan}", style: TextStyle(fontWeight: FontWeight.bold),),
                                          margin: EdgeInsets.only(bottom: 8),
                                        ),
                                        Container(
                                          alignment: Alignment.centerRight,
                                          margin: EdgeInsets.only(right: 16),
                                          child: GestureDetector(
                                            child: Icon(Icons.delete, color: Colors.pinkAccent,),
                                            onTap: () async{
                                              controller.updateReloadTrans(true);
                                              controller.deleteTransaksiItem(TransaksiModels.data[item].id, widget.addingMenu);
                                            },
                                          ),
                                        ),
                                      ],
                                    );
                                  }
                              );
                            }

                            return CircularProgressIndicator();
                        }
                      )
                  ),
                  Padding(
                    padding:EdgeInsets.symmetric(horizontal:6.0),
                    child:Container(
                      height:1.0,
                      width:double.infinity,
                      color:Colors.black,),),
                  SizedBox(height: 12,),
//                  Container(
//                    child: Row(
//                      children: <Widget>[
//                        Text("", style: TextStyle(fontWeight: FontWeight.bold),),
//                        Spacer(),
//                        Text("", style: TextStyle(fontWeight: FontWeight.bold)),
//                        Spacer(),
//                        Text("Total : ", style: TextStyle(fontWeight: FontWeight.bold)),
//                        Spacer(),
//                        GetBuilder<TransaksiController>(
//                          init: controller,
//                          builder: (params){
//                              return Text("Rp, ${params.total_tagihan.toString()}", style: TextStyle(fontWeight: FontWeight.bold));
//                          },
//                        )
//                      ],
//                    ),
//                  ),
                ],
              ),
            )
          ],
          footer: Footer(
            backgroundColor: Colors.white,
            child: FooterDashboardView(),
          ),
        ),
        floatingActionButton: FloatingActionButton(
            backgroundColor: Colors.pinkAccent,
            child: Icon(Icons.print),
            onPressed: (){
//              Get.to(PrintingDapurView(data: TransaksiModels.data,addingMenu: widget.addingMenu,));

              //code to print with this device
              printerManager.selectPrinter(controller_print.devices[0]);
              Ticket ticket=Ticket(PaperSize.mm58);

              if(!widget.addingMenu){
                ticket.text('NO TRANS : ${KasirModel.no_nota}');
                ticket.text('ANTRIAN : ${KasirModel.no_antrian}');
              }else{
                ticket.text('NO TRANS : ${KasirModel.no_nota}');
                ticket.text('TAMBAHAN : ${KasirModel.no_tambahan.replaceAll(KasirModel.no_nota, "")}');
                ticket.text('ANTRIAN : ${KasirModel.no_antrian}');
              }

              ticket.row([
                PosColumn(
                  text: 'Jml Org',
                  width: 3,
                  styles: PosStyles(align: PosTextAlign.center, underline: true),
                ),
                PosColumn(
                  text: 'Jml Ikan',
                  width: 3,
                  styles: PosStyles(align: PosTextAlign.center, underline: true),
                ),
                PosColumn(
                  text: 'Menu',
                  width: 6,
                  styles: PosStyles(align: PosTextAlign.center, underline: true),
                ),
              ]);

              if(TransaksiModels.data.length > 0){
                for(int i = 0; i < TransaksiModels.data.length; i++){
                  ticket.row([
                    PosColumn(
                      text: i == 0 ? TransaksiModels.data[i].qty_org : "-",
                      width: 3,
                      styles: PosStyles(align: PosTextAlign.center, underline: true),
                    ),
                    PosColumn(
                      text: TransaksiModels.data[i].qty_produk,
                      width: 3,
                      styles: PosStyles(align: PosTextAlign.center, underline: true),
                    ),
                    PosColumn(
                      text: TransaksiModels.data[i].jenis_produk+" "+TransaksiModels.data[i].jenis_menu,
                      width: 6,
                      styles: PosStyles(align: PosTextAlign.center, underline: true),
                    ),
                  ]);

                  ticket.row([
                    PosColumn(
                      text: "",
                      width: 3,
                      styles: PosStyles(align: PosTextAlign.center, underline: true),
                    ),
                    PosColumn(
                      text: "",
                      width: 3,
                      styles: PosStyles(align: PosTextAlign.center, underline: true),
                    ),
                    PosColumn(
                      text: TransaksiModels.data[i].keterangan,
                      width: 6,
                      styles: PosStyles(align: PosTextAlign.center, underline: true),
                    ),
                  ]);
                }
              }

              ticket.feed(1);
              ticket.cut();

              printerManager.printTicket(ticket).then((result) {
//                Scaffold.of(context).showSnackBar(SnackBar(content: Text(result.msg)));
                Get.snackbar("Informasi", result.msg, backgroundColor: Colors.green);
              }).catchError((error){
                Get.snackbar("Informasi", error.toString(), backgroundColor: Colors.red);
//                Scaffold.of(context).showSnackBar(SnackBar(content: Text(error.toString())));
              });
            }
        )
    );
  }
}
