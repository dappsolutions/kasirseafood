import 'package:flutter/material.dart';


class ContentFormKasir extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return  Container(
        padding: EdgeInsets.only(left: 32, right: 32, top: 16),
        child: TextFormField(
          decoration: InputDecoration(
              prefixIcon: Icon(Icons.people),
              hintText: "Jumlah Orang",
              labelText: "Jumlah Orang"
          ),
          keyboardType: TextInputType.text,
          validator: (text){
            if(text.isEmpty){
              return "Jumlah Orang Wajib Diisi";
            }
            return null;
          },
        )
    );
  }
}
