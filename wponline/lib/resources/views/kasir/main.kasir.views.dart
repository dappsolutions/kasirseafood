import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:footer/footer.dart';
import 'package:footer/footer_view.dart';
import 'package:get/get.dart';
import 'package:wponline/app/controllers/kasir.controllers.dart';
import 'package:wponline/app/models/kasir.models.dart';
import 'package:wponline/app/models/menu.models.dart';
import 'package:wponline/app/models/produk.models.dart';
import 'package:wponline/app/storage/menu.dart';
import 'package:wponline/app/storage/produk.dart';
import 'package:wponline/app/storage/produk_has_stok.dart';
import 'package:wponline/resources/views/dashboard/footer.views.dart';
import 'package:wponline/resources/views/dashboard/title.views.dart';
import 'package:wponline/resources/views/kasir/content.form.kasir.views.dart';
import 'package:wponline/resources/views/kasir/detail.pesanan.kasir.views.dart';
import 'package:wponline/resources/views/kasir/titleform.kasir.views.dart';


class MainKasirView extends StatefulWidget {
  String no_nota = "";
  String no_antrian = "";
  bool addingmenu = false;
  MainKasirView({this.no_nota, this.no_antrian, this.addingmenu});

  @override
  _MainKasirViewState createState() => _MainKasirViewState();
}

class _MainKasirViewState extends State<MainKasirView> {
  KasirController controller;

  @override
  void initState() {
    controller = KasirController();
    KasirModel.no_tambahan = "";
    controller.update();

    if(widget.no_nota == ""){
      controller.generateNoTransaksi();
      KasirModel.no_nota = "";
    }else{
      KasirModel.no_nota = widget.no_nota;
    }

    controller.getListProduk();
    controller.getListMenu();
    controller.clearData();

    if(widget.addingmenu){
      controller.generateNoTambahan();
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Form(
        child: FooterView(
          children: <Widget>[
            Container(
              child: Column(
                children: <Widget>[
                  TitleView(title: "KASIR DAPPSPOS",),
                  TitleFormView(title: "FORM KASIR"),
                  Container(
                      padding: EdgeInsets.only(left: 32, right: 32, top: 24),
                      child: GetBuilder<KasirController>(
                          init: controller,
                          builder: (params){
                            if(KasirModel.no_nota != ""){
                              return TextFormField(
                                decoration: InputDecoration(
                                    prefixIcon: Icon(Icons.filter_none),
                                    border: OutlineInputBorder(),
                                    hintText: "NOMOR TRANSAKSI",
                                    labelText: "NOMOR TRANSAKSI"
                                ),
                                enabled: false,
                                initialValue: KasirModel.no_nota,
                                keyboardType: TextInputType.text,
                                validator: (text){
                                  if(text.isEmpty){
                                    return "NOMOR ANTRIAN Wajib Diisi";
                                  }
                                  return null;
                                },
                                onChanged: (val){
                                  KasirModel.no_nota = val.toString();
                                },
                              );
                            }

                            return CircularProgressIndicator();
                          }

                      )
                  ),
                  Container(
                      padding: EdgeInsets.only(left: 32, right: 32, top: 24),
                      child: !widget.addingmenu ? Text("") : GetBuilder<KasirController>(
                          init: controller,
                          builder: (params){
                            if(KasirModel.no_tambahan != ""){
                              return TextFormField(
                                decoration: InputDecoration(
                                    prefixIcon: Icon(Icons.filter_none),
                                    border: OutlineInputBorder(),
                                    hintText: "NOMOR TAMBAHAN MENU",
                                    labelText: "NOMOR TAMBAHAN MENU"
                                ),
                                enabled: false,
                                initialValue: KasirModel.no_tambahan,
                                keyboardType: TextInputType.text,
                                validator: (text){
                                  if(text.isEmpty){
                                    return "NOMOR TAMBAHAN Wajib Diisi";
                                  }
                                  return null;
                                },
                                onChanged: (val){
                                  KasirModel.no_tambahan = val.toString();
                                },
                              );
                            }

                            return CircularProgressIndicator();
                          }

                      )
                  ),
                  Container(
                      padding: EdgeInsets.only(left: 32, right: 32, top: 16),
                      child: TextFormField(
                        decoration: InputDecoration(
                            prefixIcon: Icon(Icons.filter_none),
                            hintText: "No Antrian",
                            labelText: "No Antrian"
                        ),
                        keyboardType: TextInputType.number,
                        enabled: widget.no_nota == "" ? true : false,
                        initialValue: KasirModel.no_antrian,
                        validator: (text){
                          if(text.isEmpty){
                            return "No Antrian Wajib Diisi";
                          }
                          return null;
                        },
                        onChanged: (val){
                          KasirModel.no_antrian = val.toString();
                          KasirModel.params["no_antrian"] = val.toString();
                        },
                      )
                  ),
                  Container(
                      padding: EdgeInsets.only(left: 32, right: 32, top: 16),
                      child: TextFormField(
                        decoration: InputDecoration(
                            prefixIcon: Icon(Icons.people),
                            hintText: "Jumlah Orang",
                            labelText: "Jumlah Orang"
                        ),
                        keyboardType: TextInputType.number,
                        initialValue: KasirModel.qty_org != "" ? KasirModel.qty_org : "",
//                        enabled: KasirModel.qty_org != "" ? false : true,
                        validator: (text){
                          if(text.isEmpty){
                            return "Jumlah Orang Wajib Diisi";
                          }
                          return null;
                        },
                        onChanged: (val){
                          KasirModel.params["jml_org"] = val.toString();
                        },
                      )
                  ),
                  Container(
                      padding: EdgeInsets.only(left: 32, right: 32, top: 16),
                      child: GetBuilder<KasirController>(
                          init: controller,
                          builder: (params){
                            if(KasirModel.jml_ikan != ""){
                              return TextFormField(
                                decoration: InputDecoration(
                                    prefixIcon: Icon(Icons.confirmation_number),
                                    hintText: "Jumlah Ikan",
                                    labelText: "Jumlah Ikan"
                                ),
                                keyboardType: TextInputType.number,
                                validator: (text){
                                  if(text.isEmpty){
                                    return "Jumlah Ikan Wajib Diisi";
                                  }
                                  return null;
                                },
                                onChanged: (val){
                                  KasirModel.jml_ikan = val.toString();
                                },
                              );
                            }

                            return TextFormField(
                              decoration: InputDecoration(
                                  prefixIcon: Icon(Icons.confirmation_number),
                                  hintText: "Jumlah Ikan",
                                  labelText: "Jumlah Ikan"
                              ),
                              keyboardType: TextInputType.number,
                              initialValue: "",
                              validator: (text){
                                if(text.isEmpty){
                                  return "Jumlah Ikan Wajib Diisi";
                                }
                                return null;
                              },
                              onChanged: (val){
                                KasirModel.jml_ikan = val.toString();
                              },
                            );
                          }
                      )
                  ),
                  Container(
                      padding: EdgeInsets.only(left: 32, right: 32, top: 16),
                      child: TextFormField(
                        decoration: InputDecoration(
                            prefixIcon: Icon(Icons.confirmation_number),
                            hintText: "Berat Ikan (Kg)",
                            labelText: "Berat Ikan (Kg)"
                        ),
                        keyboardType: TextInputType.number,
                        validator: (text){
                          if(text.isEmpty){
                            return "Berat Ikan (Kg) Wajib Diisi";
                          }
                          return null;
                        },
                        onChanged: (val){
//                          KasirModel.params["berat_ikan"] = val.toString();
                            KasirModel.berat_ikan = val.toString();
                        },
                      )
                  ),
                  Container(
                      padding: EdgeInsets.only(left: 32, right: 32, top: 16),
                      height: 80,
                      child: GetBuilder<KasirController>(
                          init: controller,
                          builder: (params){
                            if(KasirModel.default_tipe_produk != ""){
                              return InputDecorator(
                                  decoration: const InputDecoration(border: OutlineInputBorder()),
                                  child: DropdownButtonHideUnderline(
                                      child: DropdownButton(
                                        items: ProdukModel.data_stok
                                            .map((Produk_has_stok value) {
                                          return DropdownMenuItem<String>(
                                            value: value.id,
                                            child: Text(value.jenis+" - Sisa Stok : ${value.stok_outstanding} Kg"),
                                          );
                                        }).toList(),
                                        value: KasirModel.default_tipe_produk,
                                        onChanged: (val) {
                                          controller.updateTipeProduk(val.toString());
                                        },
                                      )
                                  )
                              );
                            }

                            return CircularProgressIndicator();
                          }
                      )
                  ),
                  Container(
                      padding: EdgeInsets.only(left: 32, right: 32, top: 16),
                      height: 80,
                      child: GetBuilder<KasirController>(
                        init: controller,
                        builder: (params){
                          if(KasirModel.default_tipe_menu != ""){
                            return InputDecorator(
                                decoration: const InputDecoration(border: OutlineInputBorder()),
                                child: DropdownButtonHideUnderline(
                                    child: DropdownButton(
                                      items: MenuModel.data
                                          .map((Menu value) {
                                        return DropdownMenuItem<String>(
                                          value: value.id,
                                          child: Text(value.jenis),
                                        );
                                      }).toList(),
                                      value: KasirModel.default_tipe_menu,
                                      onChanged: (val) {
                                        controller.updateMenu(val.toString());
                                      },
                                    )
                                )
                            );
                          }

                          return CircularProgressIndicator();
                        },
                      )
                  ),
                  Container(
                      padding: EdgeInsets.only(left: 32, right: 32, top: 16),
                      child: TextFormField(
                        decoration: InputDecoration(
                            prefixIcon: Icon(Icons.text_fields),
                            hintText: "Keterangan (Makan Di Dalam, Bungkus, Dll)",
                            labelText: "Keterangan"
                        ),
                        keyboardType: TextInputType.text,
                        validator: (text){
                          if(text.isEmpty){
                            return "Keterangan  Wajib Diisi";
                          }
                          return null;
                        },
                        onChanged: (val){
                          KasirModel.keterangan = val.toString();
                        },
                      )
                  ),
                  Container(
                      padding: EdgeInsets.only(left: 32, right: 32, top: 32),
                      child: GetBuilder<KasirController>(
                        init: controller,
                        builder:(params){
                          if(KasirModel.total_item != "0"){
                            return TextFormField(
                              decoration: InputDecoration(
                                  prefixIcon: Icon(Icons.filter_none),
                                  border: OutlineInputBorder(),
                                  hintText: "TOTAL PESANAN",
                                  labelText: "TOTAL PESANAN"
                              ),
//                              enabled: false,
                              initialValue: KasirModel.total_item,
                              keyboardType: TextInputType.number,
                              validator: (text){
                                if(text.isEmpty){
                                  return "TOTAL PESANAN Wajib Diisi";
                                }
                                return null;
                              },
                            );
                          }
//
                          return Text("");
                        }
                      )
                  ),
                  Container(
                      padding: EdgeInsets.only(left: 32, right: 32, top: 16),
                      width: double.infinity,
                      height: 70,
                      child: RaisedButton(
                        child: Text("SELESAI PROSES", style: TextStyle(color: Colors.white),),
                        color: Colors.green,
                        onPressed: () async{
                          bool gotoDapur = true;

                          if(!widget.addingmenu){
                            if(widget.no_nota != ""){
                              gotoDapur = false;
                              Get.back(result: "reload");
                            }
                          }
                          controller.selesaiProses(gotoDapur, widget.addingmenu);
                        },
                        elevation: 8,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(8.0),
                        ),
                      ),

                  ),
                  Container(
                    padding: EdgeInsets.only(left: 32, right: 32, top: 8),
                    width: double.infinity,
                    height: 70,
                    child: RaisedButton(
                      child: Text("BATAL PESAN", style: TextStyle(color: Colors.black87),),
                      color: Colors.white,
                      onPressed: (){
                          Get.back();
                      },
                      elevation: 8,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(8.0),
                      ),
                    ),

                  ),
                ],
              ),
            ),
          ],
          footer: Footer(
            backgroundColor: Colors.white,
            child: FooterDashboardView(),
          ),
        ),

      ),
      floatingActionButton: FloatingActionButton(
          backgroundColor: Colors.pinkAccent,
          child: Icon(Icons.check),
          onPressed: () async{
              KasirModel.params["jml_ikan"] = KasirModel.jml_ikan;
              KasirModel.params["berat_ikan"] = KasirModel.berat_ikan;
              KasirModel.params["keterangan"] = KasirModel.keterangan;
              controller.clearData();
              await controller.simpan();

          })
    );
  }
}
