import 'package:flutter/material.dart';

class TitleFormView extends StatelessWidget {
  String title;
  TitleFormView({this.title});

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(left: 30, right: 32, top: 32),
      child: Row(
        children: <Widget>[
          Container(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(this.title, style: TextStyle(fontSize: 24, fontWeight: FontWeight.bold),),
                  SizedBox(
                    height: 5,
                  ),
                ],
              )
          ),
        ],
      ),
    );
  }
}
