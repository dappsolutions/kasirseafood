import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:search_page/search_page.dart';
import 'package:wponline/app/controllers/antrian.controllers.dart';
import 'package:wponline/app/models/kasir.models.dart';
import 'package:wponline/app/models/tagihan.models.dart';
import 'package:wponline/app/storage/produk_transaksi.dart';
import 'package:wponline/resources/views/kasir/detail.pesanan.kasir.views.dart';
import 'package:wponline/resources/views/kasir/main.kasir.views.dart';


class ListAntrianView extends StatefulWidget {
  @override
  _ListAntrianViewState createState() => _ListAntrianViewState();
}

class _ListAntrianViewState extends State<ListAntrianView> {
  AntrianController controller;

  @override
  void initState() {
    controller = new AntrianController();
    controller.getListAntrianTagihan();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("ANTRIAN DAPPOS"),
      ),
      body: SingleChildScrollView(
        child: Container(
          margin: EdgeInsets.only(left: 16, right: 16),
          child: Column(
            children: <Widget>[
              Container(
                  padding: EdgeInsets.only(left: 16, right: 16),
                  child: GetBuilder<AntrianController>(
                      init: controller,
                      builder: (params){
                        if(!params.reload){
                          return ListView.builder(
                            itemCount: TagihanModels.data.length,
                            shrinkWrap: true,
                            itemBuilder: (context, index) {
                              final Produk_transaksi p_trans = TagihanModels.data[index];
                              return Container(
                                child: Column(
                                  children: <Widget>[
                                    ListTile(
                                      title: Text(p_trans.no_transaksi+"\nANTRIAN : ${p_trans.no_antrian}", style: TextStyle(fontWeight: FontWeight.bold),),
                                      subtitle: Text(p_trans.createddate),
                                      trailing: Text('Pilih', style: TextStyle(color: Colors.pinkAccent),),
                                      onTap: () async{

                                        showDialog(
                                            context: context,
                                            builder:(context){
                                              return AlertDialog(
                                                title: Text("Aksi Transaksi"),
                                                content: Container(
                                                  height: 150,
                                                  child: Column(
                                                    children: <Widget>[
                                                      ListTile(
                                                        title: Text("Edit"),
                                                        subtitle: Text("Edit Transaksi"),
                                                        onTap: () async{
                                                            KasirModel.no_nota = p_trans.no_transaksi;
                                                            KasirModel.no_antrian = p_trans.no_antrian;
                                                            KasirModel.keterangan = "";
                                                            KasirModel.qty_org = "";
                                                            KasirModel.params["jml_org"] = "";
                                                            String result = await Get.to(MainKasirView(
                                                              no_nota: KasirModel.no_nota,
                                                              addingmenu: true,
                                                            ));

                                                            if(result == "reload"){
                                                              controller.getListAntrianTagihan();
                                                            }
                                                        },
                                                      ),
                                                      ListTile(
                                                        title: Text("Detail"),
                                                        subtitle: Text("Detail Transaksi"),
                                                        onTap: (){
                                                          KasirModel.no_nota = p_trans.no_transaksi;
                                                          KasirModel.no_antrian = p_trans.no_antrian;
                                                          Get.to(DetailPesananKasirView(addingMenu: false,));
                                                        },
                                                      ),
                                                    ],
                                                  ),
                                                ),
                                              );
                                            }
                                        );
                                      },
                                    ),
                                    Container(
                                      alignment: Alignment.centerRight,
                                      margin: EdgeInsets.only(right: 16),
                                      child: GestureDetector(
                                        child: Icon(Icons.delete, color: Colors.pinkAccent,),
                                        onTap: () async{
                                          controller.deleteTransaksi(p_trans.no_transaksi);
                                        },
                                      ),
                                    ),
                                    Padding(
                                      padding:EdgeInsets.symmetric(horizontal:6.0),
                                      child:Container(
                                        height:1.0,
                                        margin: EdgeInsets.only(left: 16, right: 16, top: 8),
                                        width:double.infinity,
                                        color:Colors.grey,),),


                                  ],
                                ),
                              );
                            },
                          );
                        }

                        return Container(
                          child: Center(
                            child: CircularProgressIndicator(),
                          ),
                        );
                      }
                  )
              )
            ],
          ),
        ),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.endFloat,
      floatingActionButton: Column(
        mainAxisAlignment: MainAxisAlignment.end,
        children: <Widget>[
          Container(
            height: 80,
            child: FloatingActionButton(
                child: Icon(Icons.search),
                heroTag: "search",
                backgroundColor: Colors.pinkAccent,
                onPressed:  () => showSearch(
                  context: context,
                  delegate: SearchPage<Produk_transaksi>(
                    items: TagihanModels.data,
                    searchLabel: 'Cari No Antrian',
                    suggestion: Center(
                      child: Text('Cari Antrian Berdasarkan No. Antrian'),
                    ),
                    failure: Center(
                      child: Text('Tidak ada antrian ditemukan :('),
                    ),
                    filter: (data) => [
                      data.no_transaksi,
                      data.no_antrian,
                      data.createddate,
                    ],
                    builder: (data) => ListTile(
                      title: Text(data.no_transaksi+"\nANTRIAN : ${data.no_antrian}", style: TextStyle(fontWeight: FontWeight.bold),),
                      subtitle: Text(data.createddate),
                      trailing: Text('Pilih', style: TextStyle(color: Colors.pinkAccent),),
                      onTap: () async{
                        showDialog(
                            context: context,
                            builder:(context){
                              return AlertDialog(
                                title: Text("Aksi Transaksi"),
                                content: Container(
                                  height: 150,
                                  child: Column(
                                    children: <Widget>[
                                      ListTile(
                                        title: Text("Edit"),
                                        subtitle: Text("Edit Transaksi"),
                                        onTap: () async{
                                          KasirModel.no_nota = data.no_transaksi;
                                          KasirModel.no_antrian = data.no_antrian;
                                          KasirModel.keterangan = "";
                                          KasirModel.qty_org = "";
                                          KasirModel.params["jml_org"] = "";
                                          String result = await Get.to(MainKasirView(
                                            no_nota: KasirModel.no_nota,
                                            addingmenu: true,
                                          ));

                                          if(result == "reload"){
                                            controller.getListAntrianTagihan();
                                          }
                                        },
                                      ),
                                      ListTile(
                                        title: Text("Detail"),
                                        subtitle: Text("Detail Transaksi"),
                                        onTap: (){
                                          KasirModel.no_nota = data.no_transaksi;
                                          KasirModel.no_antrian = data.no_antrian;
                                          Get.to(DetailPesananKasirView(addingMenu: false,));
                                        },
                                      ),
                                    ],
                                  ),
                                ),
                              );
                            }
                        );
                      },
                    ),
                  ),
                ),
            )

          ),
          Container(
            height: 80,
            child: FloatingActionButton(
                child: Icon(Icons.add),
                backgroundColor: Colors.pinkAccent,
                heroTag: "add",
                onPressed: (){
                  KasirModel.keterangan = "";
                  KasirModel.qty_org = "";
                  Get.to(MainKasirView(no_nota: "",addingmenu: false,));
                })
          ),
        ],
      ),
    );
  }
}
