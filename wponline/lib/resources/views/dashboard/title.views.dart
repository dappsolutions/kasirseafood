import 'package:flutter/material.dart';


class TitleView extends StatelessWidget {
  String title;
  TitleView({this.title});


  @override
  Widget build(BuildContext context) {
    return Container(
      child: Center(
          child: Text(this.title, style: TextStyle(fontSize: 24, color: Colors.white, fontWeight: FontWeight.bold),)
      ),
      padding: EdgeInsets.all(16),
      width: double.infinity,
      color: Colors.blue,
    );
  }
}
