import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:wponline/resources/views/menumasakan/main.menumasakan.views.dart';
import 'package:wponline/resources/views/tipeikan/main.tipeikan.views.dart';


class MenuDashboardPertama extends StatelessWidget {
  Color hexToColor(String code) {
    return new Color(int.parse(code.substring(1, 7), radix: 16) + 0xFF000000);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 160,
      margin: EdgeInsets.only(top: 80),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Container(
            decoration: BoxDecoration(
                border: Border.all(color: Colors.grey, width: 1, style: BorderStyle.solid),
                borderRadius: BorderRadius.circular(8)
            ),
            margin: EdgeInsets.only(left: 6, right: 6),
            padding: EdgeInsets.all(16),
            child: Column(
              children: <Widget>[
                GestureDetector(
                  child: Image(
                    image: AssetImage("logo_ikan.png"),
                    fit: BoxFit.cover,
                    width: 100,
                    height: 100,
                  ),
                  onTap: (){
                      Get.to(MainTipeIkanView());
                  },
                ),
                SizedBox(
                  height: 8,
                ),
                Text("IKAN", style: TextStyle(color: hexToColor("#000000")),)
              ],
            ),
          ),
          Container(
            decoration: BoxDecoration(
                border: Border.all(color: Colors.grey, width: 1, style: BorderStyle.solid),
                borderRadius: BorderRadius.circular(8)
            ),
            margin: EdgeInsets.only(left: 6, right: 6),
            padding: EdgeInsets.all(16),
            child: Column(
              children: <Widget>[
                GestureDetector(
                  child: Image(
                    image: AssetImage("logo_menu.png"),
                    fit: BoxFit.cover,
                    width: 100,
                    height: 100,
                  ),
                  onTap: (){
                      Get.to(MainMenuMasakanView());
                  },
                ),
                SizedBox(
                  height: 8,
                ),
                Text("MENU", style: TextStyle(color: hexToColor("#000000")),)
              ],
            ),
          ),
        ],
      ),
    );
  }
}