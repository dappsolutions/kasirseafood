import 'package:flutter/material.dart';
import 'package:footer/footer.dart';

class FooterDashboardView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children:<Widget>[
            Text('Copyright ©2020, All Rights Reserved.',style: TextStyle(fontWeight:FontWeight.w300, fontSize: 12.0, color: Color(0xFF162A49)),),
            Text('Dappsolutions',style: TextStyle(fontWeight:FontWeight.w300, fontSize: 12.0,color: Color(0xFF162A49)),),
          ]
      );
  }
}
