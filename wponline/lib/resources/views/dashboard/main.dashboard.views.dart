import 'package:flutter/material.dart';
import 'package:footer/footer.dart';
import 'package:footer/footer_view.dart';
import 'package:wponline/app/models/login.models.dart';
import 'package:wponline/resources/views/dashboard/footer.views.dart';
import 'package:wponline/resources/views/dashboard/menu.dashboard.views.dart';
import 'package:wponline/resources/views/dashboard/menu.second.dashboard.views.dart';
import 'package:wponline/resources/views/dashboard/menu.third.dashboard.views.dart';
import 'package:wponline/resources/views/dashboard/title.views.dart';

class MainDashboardView extends StatefulWidget {
  @override
  _MainDashboardViewState createState() => _MainDashboardViewState();
}

class _MainDashboardViewState extends State<MainDashboardView> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
//      appBar: AppBar(
//        title: Text("Dashboard"),
//        centerTitle: true,
//      ),
      body: FooterView(
        children: <Widget>[
          Container(
              child: Column(
                children: <Widget>[
                  TitleView(title: "DAPPSPOS",),
                  SizedBox(
                    height: 10,
                  ),
                  Center(
                    child: Text(LoginModel.hak_akses.toUpperCase(), style: TextStyle(fontSize: 18),),
                  ),
                  MenuDashboardPertama(),
                  MenuDashboardKedua(),
                  MenuDashboardKetiga()
                ],
              )
          ),
        ],
        footer: Footer(backgroundColor: Colors.white,child: FooterDashboardView()),
      )
    );
  }
}
