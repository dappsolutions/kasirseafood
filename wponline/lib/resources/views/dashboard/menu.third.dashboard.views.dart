import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:wponline/app/models/login.models.dart';
import 'package:wponline/resources/views/laporan/main.laporan.views.dart';
import 'package:wponline/resources/views/laporan/main.laporanbulanan.views.dart';
import 'package:wponline/resources/views/laporan/main.laporanpenambahanstok.views.dart';
import 'package:wponline/resources/views/laporan/main.lapstok.views.dart';
import 'package:wponline/resources/views/tagihan/main.tagihan.views.dart';


class MenuDashboardKetiga extends StatelessWidget {
  Color hexToColor(String code) {
    return new Color(int.parse(code.substring(1, 7), radix: 16) + 0xFF000000);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 160,
      margin: EdgeInsets.only(top: 8),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Container(
            decoration: BoxDecoration(
                border: Border.all(color: Colors.grey, width: 1, style: BorderStyle.solid),
                borderRadius: BorderRadius.circular(8)
            ),
            margin: EdgeInsets.only(left: 6, right: 6),
            padding: EdgeInsets.all(16),
            child: Column(
              children: <Widget>[
                GestureDetector(
                  child: Image(
                    image: AssetImage("logo_kasir.png"),
                    fit: BoxFit.cover,
                    width: 100,
                    height: 100,
                  ),
                  onTap: (){
                    if(LoginModel.hak_akses == "dapur"){
                      Get.snackbar("Informasi", "Anda Tidak Mempunyai Hak Akses Untuk Menu Ini");
                    }else{
                      Get.to(MainTagihanView());
                    }
                  },
                ),
                SizedBox(
                  height: 8,
                ),
                Text("TAGIHAN", style: TextStyle(color: hexToColor("#000000")),)
              ],
            ),
          ),
          Container(
            decoration: BoxDecoration(
                border: Border.all(color: Colors.grey, width: 1, style: BorderStyle.solid),
                borderRadius: BorderRadius.circular(8)
            ),
            padding: EdgeInsets.all(16),
            margin: EdgeInsets.only(left: 6, right: 6),
            child: Column(
              children: <Widget>[
                GestureDetector(
                  child: Image(
                    image: AssetImage("logo_report.png"),
                    fit: BoxFit.cover,
                    width: 100,
                    height: 100,
                  ),
                  onTap: (){
                      showDialog(
                          context: context,
                        builder:(context){
                              return AlertDialog(
                                title: Text("Daftar Laporan"),
                                content: Container(
                                  height: 300,
                                  child: Column(
                                    children: <Widget>[
                                      ListTile(
                                        title: Text("Laporan Transaksi"),
                                        subtitle: Text("Detail Transaksi Masuk"),
                                        onTap: (){
                                          if(LoginModel.hak_akses == "admin"){
                                            Get.to(MainLaporanView());
                                          }else{
                                            Get.snackbar("Informasi", "Anda Tidak Mempunyai Hak Akses Untuk Menu Ini");
                                          }
                                        },
                                      ),
                                      ListTile(
                                        title: Text("Laporan Transaksi Bulanan"),
                                        subtitle: Text("Detail Transaksi Masuk Bulanan"),
                                        onTap: (){
                                          if(LoginModel.hak_akses == "admin"){
                                            Get.to(MainLaporanBulananView());
                                          }else{
                                            Get.snackbar("Informasi", "Anda Tidak Mempunyai Hak Akses Untuk Menu Ini");
                                          }
                                        },
                                      ),
                                      ListTile(
                                        title: Text("Laporan Stok Keluar"),
                                        subtitle: Text("Detail Data Stok Keluar"),
                                        onTap: (){
                                          if(LoginModel.hak_akses == "admin"){
                                            Get.to(MainLaporanStokView());
                                          }else{
                                            Get.snackbar("Informasi", "Anda Tidak Mempunyai Hak Akses Untuk Menu Ini");
                                          }
                                        },
                                      ),
                                      ListTile(
                                        title: Text("Laporan Penambahan Stok"),
                                        subtitle: Text("Detail Data Penambahan Stok"),
                                        onTap: (){
                                          if(LoginModel.hak_akses == "admin"){
                                            Get.to(MainLaporanPenambahanStokView());
                                          }else{
                                            Get.snackbar("Informasi", "Anda Tidak Mempunyai Hak Akses Untuk Menu Ini");
                                          }
                                        },
                                      ),
                                    ],
                                  ),
                                ),
                              );
                        }
                      );
                  },
                ),
                SizedBox(
                  height: 8,
                ),
                Text("LAPORAN", style: TextStyle(color: hexToColor("#000000")),)
              ],
            ),
          ),
        ],
      ),
    );
  }
}