import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:wponline/resources/views/kasir/listantrian.views.dart';
import 'package:wponline/resources/views/kasir/main.kasir.views.dart';
import 'package:wponline/resources/views/printing/main.printing.views.dart';
import 'package:wponline/resources/views/tentang/tentang.views.dart';


class MenuDashboardKedua extends StatelessWidget {
  Color hexToColor(String code) {
    return new Color(int.parse(code.substring(1, 7), radix: 16) + 0xFF000000);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 160,
      margin: EdgeInsets.only(top: 8),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Container(
            decoration: BoxDecoration(
                border: Border.all(color: Colors.grey, width: 1, style: BorderStyle.solid),
                borderRadius: BorderRadius.circular(8)
            ),
            margin: EdgeInsets.only(left: 6, right: 6),
            padding: EdgeInsets.all(16),
            child: Column(
              children: <Widget>[
                GestureDetector(
                  child: Image(
                    image: AssetImage("logo_order.png"),
                    fit: BoxFit.cover,
                    width: 100,
                    height: 100,
                  ),
                  onTap: (){
//                      Get.to(MainKasirView(no_nota: "",));
                    Get.to(ListAntrianView());
                  },
                ),
                SizedBox(
                  height: 8,
                ),
                Text("ORDER", style: TextStyle(color: hexToColor("#000000")),)
              ],
            ),
          ),
          Container(
            decoration: BoxDecoration(
              border: Border.all(color: Colors.grey, width: 1, style: BorderStyle.solid),
              borderRadius: BorderRadius.circular(8)
            ),
            padding: EdgeInsets.all(16),
            margin: EdgeInsets.only(left: 6, right: 6),
            child: Column(
              children: <Widget>[
                GestureDetector(
                  child: Image(
                    image: AssetImage("logo_tentang.png"),
                    fit: BoxFit.cover,
                    width: 100,
                    height: 100,
                  ),
                  onTap: (){
//                      Get.to(PrintingView(data_trans: [],data_lain: [],));
                      Get.to(TentangView());
                  },
                ),
                SizedBox(
                  height: 8,
                ),
                Text("TENTANG", style: TextStyle(color: hexToColor("#000000")),)
              ],
            ),
          ),
        ],
      ),
    );
  }
}