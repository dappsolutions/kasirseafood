
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:wponline/app/controllers/laporan.controllers.dart';
import 'package:wponline/app/models/laporan.models.dart';
import 'package:wponline/app/storage/produk_transaksi_summary.dart';


class MainListLapBulananViews extends StatefulWidget {
  String bulan;
  String tahun;

  MainListLapBulananViews({this.bulan, this.tahun});


  @override
  _MainListLapBulananViewsState createState() => _MainListLapBulananViewsState();
}

class _MainListLapBulananViewsState extends State<MainListLapBulananViews> {
  LaporanController controller;

  @override
  void initState() {
    controller = new LaporanController();
    controller.getListLaporanBulanan(widget.bulan, widget.tahun);

    super.initState();
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: Text("LAPORAN TRANSAKSI"),
      ),
      body:  SingleChildScrollView(
        child: Container(
            margin: EdgeInsets.only(left: 32, right:32),
            child: Column(
              children: <Widget>[
                GetBuilder<LaporanController>(
                    init: controller,
                    builder: (params){
                      if(!params.reload){
                        return ListView.builder(
                          itemCount: LaporanModel.data.length,
                          shrinkWrap: true,
                          itemBuilder: (context, index) {
                            final Produk_transaksi_summary p_trans = LaporanModel.data[index];
                            return Container(
                              child: Column(
                                children: <Widget>[
                                  ListTile(
                                    title: Text(p_trans.no_transaksi+"\nANTRIAN : ${p_trans.no_antrian}", style: TextStyle(fontWeight: FontWeight.bold),),
                                    subtitle: Text(p_trans.createddate),
                                    trailing: Text("Rp, "+p_trans.total.toString(), style: TextStyle(color: Colors.pinkAccent),),
                                    onTap: (){

                                    },
                                  ),
                                  Padding(
                                    padding:EdgeInsets.symmetric(horizontal:6.0),
                                    child:Container(
                                      height:1.0,
                                      margin: EdgeInsets.only(left: 16, right: 16, top: 8),
                                      width:double.infinity,
                                      color:Colors.grey,),),

                                ],
                              ),
                            );
                          },
                        );
                      }

                      return Container(
                        child: Center(
                          child: CircularProgressIndicator(),
                        ),
                      );
                    }
                )
              ],
            )
        ),
      ),
      bottomSheet: Container(
          margin: EdgeInsets.only(left: 16, right: 16),
          child: GetBuilder<LaporanController>(
            init: controller,
            builder: (params){
              return Text("TOTAL PENGHASILAN : Rp, ${params.total_penghasilan}", style: TextStyle(fontWeight: FontWeight.bold, color: Colors.green),);
            },
          )
      ),
    );
  }
}
