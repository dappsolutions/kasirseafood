import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:wponline/app/controllers/laporan.controllers.dart';
import 'package:wponline/app/models/laporan.models.dart';
import 'package:wponline/app/storage/produk_stok.dart';
import 'package:wponline/app/storage/produk_transaksi_summary.dart';


class MainLaporanStokView extends StatefulWidget {
  @override
  _MainLaporanStokViewState createState() => _MainLaporanStokViewState();
}

class _MainLaporanStokViewState extends State<MainLaporanStokView> {
  LaporanController controller;

  @override
  void initState() {
    controller = new LaporanController();
    controller.getLaporanStok();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: Text("LAPORAN STOK"),
      ),
      body:  Container(
          child: GetBuilder<LaporanController>(
              init: controller,
              builder: (params){
                if(!params.reload){
                  return ListView.builder(
                    itemCount: LaporanModel.data_stok.length,
                    shrinkWrap: true,
                    itemBuilder: (context, index) {
                      final Produk_stok p_trans = LaporanModel.data_stok[index];
                      return Container(
                        child: Column(
                          children: <Widget>[
                            ListTile(
                              title: Text(p_trans.menu+"\nJUMLAH : ${p_trans.total_qty_produk}\nBERAT : ${p_trans.total_berat_produk}", style: TextStyle(fontWeight: FontWeight.bold),),
                              subtitle: Text("Rp, "+p_trans.harga_per_kg+"/Kg"),
                              trailing: Text("Rp, "+p_trans.total_harga.toString(), style: TextStyle(color: Colors.pinkAccent),),
                              onTap: (){

                              },
                            ),
                            SizedBox(
                              height: 12,
                            )
//                            Container(
//                              alignment: Alignment.centerRight,
//                              margin: EdgeInsets.only(right: 16),
//                              child: GestureDetector(
//                                child: Icon(Icons.delete, color: Colors.pinkAccent,),
//                                onTap: () async{
//                                  controller.deleteLaporan(p_trans.no_transaksi);
//                                },
//                              ),
//                            ),
//                            Padding(
//                              padding:EdgeInsets.symmetric(horizontal:6.0),
//                              child:Container(
//                                height:1.0,
//                                margin: EdgeInsets.only(left: 16, right: 16, top: 8),
//                                width:double.infinity,
//                                color:Colors.grey,),),

                          ],
                        ),
                      );
                    },
                  );
                }

                return Container(
                  child: Center(
                    child: CircularProgressIndicator(),
                  ),
                );
              }
          )
      ),
      bottomSheet: Container(
          margin: EdgeInsets.only(left: 16, right: 16),
          child: GetBuilder<LaporanController>(
            init: controller,
            builder: (params){
              return Text("TOTAL TERJUAL : Rp, ${params.total_terjual}", style: TextStyle(fontWeight: FontWeight.bold, color: Colors.green),);
            },
          )
      ),
    );
  }
}
