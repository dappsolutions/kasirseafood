import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:wponline/app/controllers/laporan.controllers.dart';
import 'package:wponline/app/models/laporan.models.dart';
import 'package:wponline/app/storage/produk_transaksi_summary.dart';

import '../../../app/controllers/tagihan.controllers.dart';
import '../../../app/models/kasir.models.dart';
import '../tagihan/form.tagihan.views.dart';

class MainLaporanView extends StatefulWidget {
  @override
  _MainLaporanViewState createState() => _MainLaporanViewState();
}

class _MainLaporanViewState extends State<MainLaporanView> {
  LaporanController controller;
  TagihanController controllerTagihan;

  @override
  void initState() {
    controller = new LaporanController();
    controller.getListLaporan();

    controllerTagihan = new TagihanController();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: Text("LAPORAN TRANSAKSI"),
      ),
      body:  SingleChildScrollView(
        child: Container(
            margin: EdgeInsets.only(left: 32, right:32),
            child: Column(
              children: <Widget>[
                Container(
                  margin: EdgeInsets.only(left: 16, right: 16, top: 8),
                  width: double.infinity,
                  height: 50,
                  child: RaisedButton(
                    child: Text("HAPUS SEMUA", style: TextStyle(color: Colors.white),),
                    color: Colors.red,
                    onPressed: (){
                      controller.deleteAllLaporan();
                    },
                    elevation: 3,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(8.0),
                    ),
                  ),
                ),
                GetBuilder<LaporanController>(
                    init: controller,
                    builder: (params){
                      if(!params.reload){
                        return ListView.builder(
                          itemCount: LaporanModel.data.length,
                          shrinkWrap: true,
                          itemBuilder: (context, index) {
                            final Produk_transaksi_summary p_trans = LaporanModel.data[index];
                            return Container(
                              child: Column(
                                children: <Widget>[
                                  ListTile(
                                    title: Text(p_trans.no_transaksi+"\nANTRIAN : ${p_trans.no_antrian}", style: TextStyle(fontWeight: FontWeight.bold),),
                                    subtitle: Text(p_trans.createddate),
                                    trailing: Text("Rp, "+p_trans.total.toString(), style: TextStyle(color: Colors.pinkAccent),),
                                    onTap: (){
                                      KasirModel.no_nota = p_trans.no_transaksi;
                                      KasirModel.no_antrian = p_trans.no_antrian;
                                      KasirModel.qty_org = "";
                                      Get.to(FormTagihanView(controller: controllerTagihan,));
                                    },
                                  ),
                                  Container(
                                    alignment: Alignment.centerRight,
                                    margin: EdgeInsets.only(right: 16),
                                    child: GestureDetector(
                                      child: Icon(Icons.delete, color: Colors.pinkAccent,),
                                      onTap: () async{
                                        controller.deleteLaporan(p_trans.no_transaksi);
                                      },
                                    ),
                                  ),
                                  Padding(
                                    padding:EdgeInsets.symmetric(horizontal:6.0),
                                    child:Container(
                                      height:1.0,
                                      margin: EdgeInsets.only(left: 16, right: 16, top: 8),
                                      width:double.infinity,
                                      color:Colors.grey,),),

                                ],
                              ),
                            );
                          },
                        );
                      }

                      return Container(
                        child: Center(
                          child: CircularProgressIndicator(),
                        ),
                      );
                    }
                )
              ],
            )
        ),
      ),
      bottomSheet: Container(
        margin: EdgeInsets.only(left: 16, right: 16),
        child: GetBuilder<LaporanController>(
          init: controller,
          builder: (params){
            return Text("TOTAL PENGHASILAN : Rp, ${params.total_penghasilan}", style: TextStyle(fontWeight: FontWeight.bold, color: Colors.green),);
          },
        )
      ),
    );
  }
}
