import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:wponline/app/models/laporan.models.dart';
import 'package:wponline/resources/views/laporan/main.listlapbulanan.views.dart';

class MainLaporanBulananView extends StatefulWidget {
  @override
  _MainLaporanBulananViewState createState() => _MainLaporanBulananViewState();
}

class _MainLaporanBulananViewState extends State<MainLaporanBulananView> {
  String bulan = "Januari";
  String tahun = "2020";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: Text("LAPORAN TRANSAKSI BULANAN"),
      ),
      body:  SingleChildScrollView(
        child: Container(
            margin: EdgeInsets.only(left: 32, right:32, top: 32),
            child: Column(
              children: <Widget>[
                Container(
                  height: 60,
                  child: InputDecorator(
                      decoration: const InputDecoration(border: OutlineInputBorder()),
                      child: DropdownButtonHideUnderline(
                          child: DropdownButton(
                            items: LaporanModel.data_tahun
                                .map((String value) {
                              return DropdownMenuItem<String>(
                                value: value,
                                child: Text(value),
                              );
                            }).toList(),
                            value:this.tahun,
                            onChanged: (val) {
                              setState(() {
                                this.tahun = val.toString();
                              });
                            },
                          )
                      )
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                Container(
                    height: 60,
                    child: InputDecorator(
                        decoration: const InputDecoration(border: OutlineInputBorder()),
                        child: DropdownButtonHideUnderline(
                            child: DropdownButton(
                              items: LaporanModel.data_bulan
                                  .map((String value) {
                                return DropdownMenuItem<String>(
                                  value: value,
                                  child: Text(value),
                                );
                              }).toList(),
                              value: this.bulan,
                              onChanged: (val) {
                                setState(() {
                                  this.bulan = val.toString();
                                });
                              },
                            )
                        )
                    )
                ),
                SizedBox(
                  height: 20,
                ),
                Container(
                  width: double.infinity,
                  height: 50,
                  child: RaisedButton(
                    child: Text("TAMPILKAN", style: TextStyle(color: Colors.white),),
                    color: Colors.green,
                    onPressed: (){
                        Get.to(MainListLapBulananViews(
                          bulan: this.bulan,
                          tahun: this.tahun,
                        ));
                    },
                    elevation: 3,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(8.0),
                    ),
                  ),
                ),
              ],
            )
        ),
      ),
    );
  }
}

