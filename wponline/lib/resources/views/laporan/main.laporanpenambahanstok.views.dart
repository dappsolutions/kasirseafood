import 'package:esc_pos_printer/esc_pos_printer.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:wponline/app/controllers/laporan.controllers.dart';
import 'package:wponline/app/controllers/printkasir.controllers.dart';
import 'package:wponline/app/models/laporan.models.dart';
import 'package:wponline/app/models/produk.models.dart';
import 'package:wponline/app/storage/produk_stok.dart';
import 'package:wponline/app/storage/produk_tambah_stok.dart';

class MainLaporanPenambahanStokView extends StatefulWidget {
  @override
  _MainLaporanPenambahanStokViewState createState() => _MainLaporanPenambahanStokViewState();
}

class _MainLaporanPenambahanStokViewState extends State<MainLaporanPenambahanStokView> {
  LaporanController controller;
  PrinterBluetoothManager printerManager = PrinterBluetoothManager();
  PrintKasirController controller_print;

  @override
  void initState() {
    controller = new LaporanController();
    controller.getLaporanPenambahanStok();
    controller_print = new PrintKasirController();
    controller_print.cariBluetoothPrinter(printerManager);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: Text("LAPORAN PENAMBAHAN STOK"),
      ),
      body:  SingleChildScrollView(
        child: Container(
            margin: EdgeInsets.only(left: 32, right:32),
            child: Column(
              children: <Widget>[
                SizedBox(height: 32,),
                Container(
                  child: GetBuilder<PrintKasirController>(
                      init: controller_print,
                      builder: (params){
                        if(!params.scanning){
                          if(params.devices.length > 0){
                            return Text("PRINTER : ${params.devices[0].address}");
                          }
                        }

                        return Text("PRINTER : -");
                      }),
                ),
                SizedBox(height: 16,),
                GetBuilder<LaporanController>(
                    init: controller,
                    builder: (params){
                      if(!params.reload){
                        return ListView.builder(
                          itemCount: ProdukModel.data_stok_tambahan.length,
                          shrinkWrap: true,
                          itemBuilder: (context, index) {
                            final Produk_tambah_stok p_trans = ProdukModel.data_stok_tambahan[index];
                            return Container(
                              child: Column(
                                children: <Widget>[
                                  ListTile(
                                    title: Text("("+p_trans.supplier+")\n${p_trans.nama_produk}", style: TextStyle(fontWeight: FontWeight.bold),),
                                    subtitle: Text(""+p_trans.createddate+""),
                                    trailing: Text('STOK : ${p_trans.stok}'),
                                    onTap: (){

                                    },
                                  ),
                                  SizedBox(
                                    height: 12,
                                  ),
                                  Container(
                                    alignment: Alignment.centerRight,
                                    margin: EdgeInsets.only(right: 16),
                                    child: GestureDetector(
                                      child: Icon(Icons.delete, color: Colors.pinkAccent,),
                                      onTap: () async{
                                        controller.deleteLaporanStok(p_trans.id);
                                      },
                                    ),
                                  ),
                                  Padding(
                                    padding:EdgeInsets.symmetric(horizontal:6.0),
                                    child:Container(
                                      height:1.0,
                                      margin: EdgeInsets.only(left: 16, right: 16, top: 8),
                                      width:double.infinity,
                                      color:Colors.grey,),),

                                ],
                              ),
                            );
                          },
                        );
                      }

                      return Container(
                        child: Center(
                          child: CircularProgressIndicator(),
                        ),
                      );
                    }
                )
              ],
            )
        ),
      ),
      floatingActionButton: FloatingActionButton(
          child: Icon(Icons.print),
          backgroundColor: Colors.pinkAccent,
          onPressed: (){
            printerManager.selectPrinter(controller_print.devices[0]);
            Ticket ticket=Ticket(PaperSize.mm58);

            ticket.text('Laporan Tambah Stok', styles: PosStyles(
                align: PosTextAlign.center, underline: true));

            if(ProdukModel.data_stok_tambahan.length > 0){
              for(int i = 0; i < ProdukModel.data_stok_tambahan.length; i++){
                ticket.row([
                  PosColumn(
                    text: ProdukModel.data_stok_tambahan[i].supplier,
                    width: 6,
                    styles: PosStyles(align: PosTextAlign.center, underline: true),
                  ),
//                  PosColumn(
//                    text: ProdukModel.data_stok_tambahan[i].nama_produk,
//                    width: 3,
//                    styles: PosStyles(align: PosTextAlign.center, underline: true),
//                  ),
                  PosColumn(
                    text: ProdukModel.data_stok_tambahan[i].stok,
                    width: 2,
                    styles: PosStyles(align: PosTextAlign.center, underline: true),
                  ),
                  PosColumn(
                    text: ProdukModel.data_stok_tambahan[i].createddate,
                    width: 4,
                    styles: PosStyles(align: PosTextAlign.center, underline: true),
                  ),
                ]);
              }

              ticket.feed(1);
              ticket.cut();

              printerManager.printTicket(ticket).then((result) {
                Get.snackbar("Informasi", result.msg +" printing", backgroundColor: Colors.green);
              }).catchError((error){
                Get.snackbar("Informasi", error.toString(), backgroundColor: Colors.red);
              });
            }

      }),
    );
  }
}
