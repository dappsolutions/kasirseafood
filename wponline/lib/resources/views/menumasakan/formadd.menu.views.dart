
import 'package:flutter/material.dart';
import 'package:footer/footer.dart';
import 'package:footer/footer_view.dart';
import 'package:get/get.dart';
import 'package:wponline/app/controllers/menu.controllers.dart';
import 'package:wponline/app/models/menu.models.dart';
import 'package:wponline/resources/views/dashboard/footer.views.dart';
import 'package:wponline/resources/views/dashboard/title.views.dart';
import 'package:wponline/resources/views/kasir/titleform.kasir.views.dart';

class FormAddMenuView extends StatefulWidget {
  MenuController controller;
  FormAddMenuView({this.controller});

  @override
  _FormAddMenuViewState createState() => _FormAddMenuViewState();
}

class _FormAddMenuViewState extends State<FormAddMenuView> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: FooterView(
          children: <Widget>[
            TitleView(title: "DAPPSPOS",),
            TitleFormView(title: "TAMBAH MENU MASAKAN",),
            Container(
                padding: EdgeInsets.only(left: 32, right: 32, top: 16),
                child: TextFormField(
                  decoration: InputDecoration(
                      prefixIcon: Icon(Icons.confirmation_number),
                      hintText: "Menu",
                      labelText: "Menu"
                  ),
                  keyboardType: TextInputType.text,
                  validator: (text){
                    if(text.isEmpty){
                      return "Menu Wajib Diisi";
                    }
                    return null;
                  },
                  onChanged: (val){
                    MenuModel.jenis_menu = val.toString();
                  },
                )
            ),
            Container(
              padding: EdgeInsets.only(left: 32, right: 32, top: 16),
              width: double.infinity,
              height: 70,
              child: RaisedButton(
                child: Text("PROSES", style: TextStyle(color: Colors.white),),
                color: Colors.green,
                onPressed: () async{
                    Get.back();
                    widget.controller.simpan();
                },
                elevation: 8,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(8.0),
                ),
              ),

            ),
          ],
          footer: Footer(
            backgroundColor: Colors.white,
            child: FooterDashboardView(),
          ),
        )
    );
  }
}
