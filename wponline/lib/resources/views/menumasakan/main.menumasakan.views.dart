import 'package:flutter/material.dart';
import 'package:footer/footer.dart';
import 'package:footer/footer_view.dart';
import 'package:get/get.dart';
import 'package:search_page/search_page.dart';
import 'package:wponline/app/controllers/menu.controllers.dart';
import 'package:wponline/app/models/menu.models.dart';
import 'package:wponline/app/models/menumasakan.models.dart';
import 'package:wponline/app/storage/menu.dart';
import 'package:wponline/resources/views/dashboard/footer.views.dart';
import 'package:wponline/resources/views/dashboard/title.views.dart';
import 'package:wponline/resources/views/kasir/titleform.kasir.views.dart';
import 'package:wponline/resources/views/menumasakan/formadd.menu.views.dart';


class MainMenuMasakanView extends StatefulWidget {
  @override
  _MainMenuMasakanViewState createState() => _MainMenuMasakanViewState();
}

class _MainMenuMasakanViewState extends State<MainMenuMasakanView> {
  MenuController controller;

  @override
  void initState() {
    controller = new MenuController();
    controller.getListMenu();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: FooterView(
        children: <Widget>[
          TitleView(title: "DAPPSPOS",),
          TitleFormView(title: "DAFTAR MENU MASAKAN",),
          Container(
            padding: EdgeInsets.only(left: 32, right: 32, top: 8),
            width: double.infinity,
            height: 70,
            child: RaisedButton(
              child: Text("TAMBAH", style: TextStyle(color: Colors.white),),
              color: Colors.pinkAccent,
              onPressed: (){
                Get.to(FormAddMenuView(controller: controller,));
              },
              elevation: 8,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(8.0),
              ),
            ),

          ),
          Container(
            padding: EdgeInsets.only(left: 16, right: 16),
            child: GetBuilder<MenuController>(
                init: controller,
                builder: (params){
                  if(!params.reload){
                    return ListView.builder(
                      itemCount: MenuModel.data.length,
                      shrinkWrap: true,
                      itemBuilder: (context, index) {
                        final Menu p_trans = MenuModel.data[index];
                        return ListTile(
                          title: Text(p_trans.jenis, style: TextStyle(fontWeight: FontWeight.bold),),
                          subtitle: Text('-'),
                            trailing: GestureDetector(
                              child: Icon(Icons.delete, color: Colors.pinkAccent,),
                              onTap: () async{
                                controller.deleteData(p_trans.id);
                              },
                            ),
                          onTap: (){
//                    print("data ${p_trans.no_transaksi}");
                          },
                        );
                      },
                    );
                  }

                  return CircularProgressIndicator();
                }
            )
          )
        ],
        footer: Footer(
          backgroundColor: Colors.white,
          child: FooterDashboardView(),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.search),
        backgroundColor: Colors.pinkAccent,
        tooltip: "Cari Jenis",
        onPressed: () => showSearch(
          context: context,
          delegate: SearchPage<Menu>(
            items: MenuModel.data,
            searchLabel: 'Cari Jenis',
            suggestion: Center(
              child: Text('Cari Antrian Berdasarkan Jeni'),
            ),
            failure: Center(
              child: Text('Tidak ada antrian ditemukan :('),
            ),
            filter: (data) => [
              data.jenis,
            ],
            builder: (data) => ListTile(
              title: Text(data.jenis, style: TextStyle(fontWeight: FontWeight.bold),),
              subtitle: Text('-'),
              trailing: Text('MENU', style: TextStyle(color: Colors.pinkAccent),),
              onTap: (){

              },
            ),
          ),
        ),
      ),
    );
  }
}
