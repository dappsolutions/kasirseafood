import 'package:esc_pos_printer/esc_pos_printer.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:wponline/app/controllers/printdapur.controllers.dart';
import 'package:wponline/app/controllers/printkasir.controllers.dart';
import 'package:wponline/app/controllers/tagihan.controllers.dart';
import 'package:wponline/app/models/kasir.models.dart';
import 'package:wponline/app/storage/porsi.dart';
import 'package:wponline/app/storage/produk_transaksi.dart';
import 'package:wponline/app/storage/produk_transaksi_detail.dart';
import 'package:wponline/app/storage/produk_transaksi_lain.php.dart';

class PrintingView extends StatefulWidget {
  TagihanController controller;
  List<Produk_transaksi_detail> data_trans = [];
  List<Produk_transaksi_lain> data_lain = [];
  List<Porsi> data_porsi = [];
  PrintingView({this.data_trans, this.data_lain, this.controller, this.data_porsi});

  @override
  _PrintingViewState createState() => _PrintingViewState();
}

class _PrintingViewState extends State<PrintingView> {
  PrinterBluetoothManager printerManager = PrinterBluetoothManager();
//  List<PrinterBluetooth> _devices = [];
  PrintKasirController controller;

  @override
  void initState() {
    controller = new PrintKasirController();
    controller.cariBluetoothPrinter(printerManager);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("CARI PRINTER"),
      ),
      body: GetBuilder<PrintKasirController>(
        init: controller,
        builder: (params){
          if(!params.scanning){
            return ListView.builder(
              itemBuilder: (context,position)=>ListTile(
                onTap: (){
                  //code to print with this device
                  printerManager.selectPrinter(params.devices[position]);
                  Ticket ticket=Ticket(PaperSize.mm58);

                  if(widget.data_trans.length > 0) {
                    ticket.row([
                      PosColumn(
                        text: widget.data_trans[widget.data_trans.length - 1].createddate,
                        width: 12,
                        styles: PosStyles(
                            align: PosTextAlign.right, underline: true),
                      ),
                    ]);
                  }

                  ticket.text('LESEHAN SANTONG', styles: PosStyles(
                      align: PosTextAlign.center, underline: true));
                  ticket.text('+6285205022764', styles: PosStyles(
                      align: PosTextAlign.center, underline: true));
                  ticket.text('ANTRIAN : ${KasirModel.no_antrian}', styles: PosStyles(
                      align: PosTextAlign.center, underline: true));

                  ticket.text("");

                  if(widget.data_trans.length > 0){
                    for(int i = 0; i < widget.data_trans.length; i++){
                      ticket.row([
                        PosColumn(
                          text: widget.data_trans[i].berat_produk+" Kg",
                          width: 3,
                          styles: PosStyles(align: PosTextAlign.center, underline: true),
                        ),
                        PosColumn(
                          text: widget.data_trans[i].jenis_produk,
                          width: 3,
                          styles: PosStyles(align: PosTextAlign.center, underline: true),
                        ),
                        PosColumn(
                          text: widget.data_trans[i].harga,
                          width: 3,
                          styles: PosStyles(align: PosTextAlign.center, underline: true),
                        ),
                        PosColumn(
                          text: widget.data_trans[i].total_harga,
                          width: 3,
                          styles: PosStyles(align: PosTextAlign.center, underline: true),
                        ),
                      ]);
                    }
                  }


                  if(widget.data_porsi.length > 0){
                    for(int i = 0; i < widget.data_porsi.length; i++){
                      ticket.row([
                        PosColumn(
                          text: widget.data_porsi[i].jml_org,
                          width: 3,
                          styles: PosStyles(align: PosTextAlign.center, underline: true),
                        ),
                        PosColumn(
                          text: "Porsi",
                          width: 3,
                          styles: PosStyles(align: PosTextAlign.center, underline: true),
                        ),
                        PosColumn(
                          text: "10000",
                          width: 3,
                          styles: PosStyles(align: PosTextAlign.center, underline: true),
                        ),
                        PosColumn(
                          text: (int.parse(widget.data_porsi[i].jml_org.toString()) * 10000).toString(),
                          width: 3,
                          styles: PosStyles(align: PosTextAlign.center, underline: true),
                        ),
                      ]);
                    }
                  }


                  if(widget.data_lain.length > 0){
                    for(int i = 0; i < widget.data_lain.length; i++){
                      ticket.row([
                        PosColumn(
                          text: widget.data_lain[i].qty+"",
                          width: 3,
                          styles: PosStyles(align: PosTextAlign.center, underline: true),
                        ),
                        PosColumn(
                          text: widget.data_lain[i].menu,
                          width: 3,
                          styles: PosStyles(align: PosTextAlign.center, underline: true),
                        ),
                        PosColumn(
                          text: widget.data_lain[i].harga,
                          width: 3,
                          styles: PosStyles(align: PosTextAlign.center, underline: true),
                        ),
                        PosColumn(
                          text: widget.data_lain[i].total_harga,
                          width: 3,
                          styles: PosStyles(align: PosTextAlign.center, underline: true),
                        ),
                      ]);
                    }
                  }

                  ticket.text("");

                  ticket.row([
                    PosColumn(
                      text: "",
                      width: 3,
                      styles: PosStyles(align: PosTextAlign.center, underline: true),
                    ),
                    PosColumn(
                      text: "",
                      width: 3,
                      styles: PosStyles(align: PosTextAlign.center, underline: true),
                    ),
                    PosColumn(
                      text: "Total : ",
                      width: 3,
                      styles: PosStyles(align: PosTextAlign.center, underline: true),
                    ),
                    PosColumn(
                      text: widget.controller.total_tagihan.toString(),
                      width: 3,
                      styles: PosStyles(align: PosTextAlign.center, underline: true),
                    ),
                  ]);

                  ticket.row([
                    PosColumn(
                      text: "",
                      width: 3,
                      styles: PosStyles(align: PosTextAlign.center, underline: true),
                    ),
                    PosColumn(
                      text: "",
                      width: 3,
                      styles: PosStyles(align: PosTextAlign.center, underline: true),
                    ),
                    PosColumn(
                      text: "Uang Bayar : ",
                      width: 3,
                      styles: PosStyles(align: PosTextAlign.center, underline: true),
                    ),
                    PosColumn(
                      text: widget.controller.uang_bayar.toString(),
                      width: 3,
                      styles: PosStyles(align: PosTextAlign.center, underline: true),
                    ),
                  ]);

                  ticket.row([
                    PosColumn(
                      text: "",
                      width: 3,
                      styles: PosStyles(align: PosTextAlign.center, underline: true),
                    ),
                    PosColumn(
                      text: "",
                      width: 3,
                      styles: PosStyles(align: PosTextAlign.center, underline: true),
                    ),
                    PosColumn(
                      text: "Kembalian : ",
                      width: 3,
                      styles: PosStyles(align: PosTextAlign.center, underline: true),
                    ),
                    PosColumn(
                      text: widget.controller.kembalian.toString(),
                      width: 3,
                      styles: PosStyles(align: PosTextAlign.center, underline: true),
                    ),
                  ]);

                  ticket.row([
                    PosColumn(
                      text: "Terima Kasih",
                      width: 12,
                      styles: PosStyles(align: PosTextAlign.center, underline: true),
                    ),
                  ]);


                  ticket.feed(1);
                  ticket.cut();

                  printerManager.printTicket(ticket).then((result) {
                    Scaffold.of(context).showSnackBar(SnackBar(content: Text(result.msg)));
                  }).catchError((error){
                    Scaffold.of(context).showSnackBar(SnackBar(content: Text(error.toString())));
                  });
                },
                title: Text(params.devices[position].name),
                subtitle: Text(params.devices[position].address),
              ),
              itemCount: params.devices.length,
            );
          }

          return Container(
            child: Center(
              child: CircularProgressIndicator(),
            ),
          );
        },
      ),
      floatingActionButton: FloatingActionButton(onPressed: (){
        controller.cariBluetoothPrinter(printerManager);
        //Code to search for devices
//        printerManager.startScan(Duration(seconds: 4));
//        printerManager.scanResults.listen((scannedDevices) {
//          print("SCANENED DEVICE ${scannedDevices.toString()}");
//          if(scannedDevices.length == 0){
//            Get.snackbar("Device", "Tidak Ditemukan", colorText: Colors.white, backgroundColor: Colors.red);
//          }
//          setState(() {
//            _devices=scannedDevices;
//          });
//        });


      },child: Icon(Icons.search),),
    );
  }
}
