import 'package:esc_pos_printer/esc_pos_printer.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:wponline/app/controllers/printdapur.controllers.dart';
import 'package:wponline/app/models/kasir.models.dart';
import 'package:wponline/app/storage/produk_transaksi_detail.dart';


class PrintingDapurView extends StatefulWidget {
  List<Produk_transaksi_detail> data;
  bool addingMenu = false;
  PrintingDapurView({this.data, this.addingMenu});

  @override
  _PrintingDapurViewState createState() => _PrintingDapurViewState();
}

class _PrintingDapurViewState extends State<PrintingDapurView> {
  PrinterBluetoothManager printerManager = PrinterBluetoothManager();
  PrintDapurController controller;

  @override
  void initState() {
//    print("DATa ${widget.data.toString()}");
    controller = new PrintDapurController();

    controller.cariBluetoothPrinter(printerManager);
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("CARI PRINTER"),
      ),
      body: GetBuilder<PrintDapurController>(
          init: controller,
          builder: (params){

            if(!params.scanning){
              return ListView.builder(
                itemBuilder: (context,position)=>ListTile(
                  onTap: (){
                    //code to print with this device
                    printerManager.selectPrinter(params.devices[position]);
                    Ticket ticket=Ticket(PaperSize.mm58);

                    if(!widget.addingMenu){
                    ticket.text('NO TRANS : ${KasirModel.no_nota}');
                    ticket.text('ANTRIAN : ${KasirModel.no_antrian}');
                    }else{
                      ticket.text('NO TRANS : ${KasirModel.no_nota}');
                      ticket.text('TAMBAHAN : ${KasirModel.no_tambahan.replaceAll(KasirModel.no_nota, "")}');
                      ticket.text('ANTRIAN : ${KasirModel.no_antrian}');
                    }

                    ticket.row([
                      PosColumn(
                        text: 'Jml Org',
                        width: 3,
                        styles: PosStyles(align: PosTextAlign.center, underline: true),
                      ),
                      PosColumn(
                        text: 'Jml Ikan',
                        width: 3,
                        styles: PosStyles(align: PosTextAlign.center, underline: true),
                      ),
                      PosColumn(
                        text: 'Menu',
                        width: 6,
                        styles: PosStyles(align: PosTextAlign.center, underline: true),
                      ),
                    ]);

                    if(widget.data.length > 0){
                      for(int i = 0; i < widget.data.length; i++){
                        ticket.row([
                          PosColumn(
                            text: i == 0 ? widget.data[i].qty_org : "-",
                            width: 3,
                            styles: PosStyles(align: PosTextAlign.center, underline: true),
                          ),
                          PosColumn(
                            text: widget.data[i].qty_produk,
                            width: 3,
                            styles: PosStyles(align: PosTextAlign.center, underline: true),
                          ),
                          PosColumn(
                            text: widget.data[i].jenis_produk,
                            width: 6,
                            styles: PosStyles(align: PosTextAlign.center, underline: true),
                          ),
                        ]);

                        ticket.row([
                          PosColumn(
                            text: "",
                            width: 3,
                            styles: PosStyles(align: PosTextAlign.center, underline: true),
                          ),
                          PosColumn(
                            text: "",
                            width: 3,
                            styles: PosStyles(align: PosTextAlign.center, underline: true),
                          ),
                          PosColumn(
                            text: widget.data[i].keterangan,
                            width: 6,
                            styles: PosStyles(align: PosTextAlign.center, underline: true),
                          ),
                        ]);
                      }
                    }

                    ticket.feed(1);
                    ticket.cut();

                    printerManager.printTicket(ticket).then((result) {
                      Scaffold.of(context).showSnackBar(SnackBar(content: Text(result.msg)));
                    }).catchError((error){
                      Scaffold.of(context).showSnackBar(SnackBar(content: Text(error.toString())));
                    });
                  },
                  title: Text(params.devices[position].name),
                  subtitle: Text(params.devices[position].address),
                ),
                itemCount: params.devices.length,
              );
            }

            return Container(
              child: Center(
                child: CircularProgressIndicator(),
              ),
            );
          }
      ),
      floatingActionButton: FloatingActionButton(onPressed: (){
        //Code to search for devices
        controller.cariBluetoothPrinter(printerManager);

      },child: Icon(Icons.search),),
    );
  }
}
