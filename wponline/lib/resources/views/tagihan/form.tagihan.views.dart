import 'package:esc_pos_printer/esc_pos_printer.dart';
import 'package:flutter/material.dart';
import 'package:footer/footer.dart';
import 'package:footer/footer_view.dart';
import 'package:get/get.dart';
import 'package:wponline/app/controllers/printkasir.controllers.dart';
import 'package:wponline/app/controllers/tagihan.controllers.dart';
import 'package:wponline/app/models/kasir.models.dart';
import 'package:wponline/app/models/transaksi.models.dart';
import 'package:wponline/resources/views/dashboard/footer.views.dart';
import 'package:wponline/resources/views/dashboard/title.views.dart';
import 'package:wponline/resources/views/kasir/main.kasir.views.dart';
import 'package:wponline/resources/views/kasir/titleform.kasir.views.dart';
import 'package:wponline/resources/views/printing/main.printing.views.dart';
import 'package:wponline/resources/views/tagihan/form.add.pesanan.views.dart';
import 'package:wponline/resources/views/tagihan/form.bayar.views.dart';
import 'package:wponline/resources/views/tagihan/form.orgmakan.views.dart';

class FormTagihanView extends StatefulWidget {
  TagihanController controller;
  FormTagihanView({this.controller});

  @override
  _FormTagihanViewState createState() => _FormTagihanViewState();
}

class _FormTagihanViewState extends State<FormTagihanView> {
  PrinterBluetoothManager printerManager = PrinterBluetoothManager();

  PrintKasirController controller_print;
  @override
  void initState() {
    widget.controller.getListPesanan();
    controller_print = new PrintKasirController();
    controller_print.cariBluetoothPrinter(printerManager);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        body: FooterView(
          children: <Widget>[
            TitleView(title: "DETAIL KASIR DAPPSPOS",),
            TitleFormView(title: "${KasirModel.no_nota}\nANTRIAN : ${KasirModel.no_antrian}"),
            GetBuilder<PrintKasirController>(
              init: controller_print,
              builder: (params){
                if(!params.scanning){
                  if(params.devices.length > 0){
                    return TitleFormView(title: "PRINTER : ${params.devices[0].address}");
                  }
                }

                return TitleFormView(title: "PRINTER : -");
              },
            ),
            Container(
              padding: EdgeInsets.only(left: 32, right: 32, top: 16),
//            height: 80,
              child: Column(
                children: <Widget>[
                  SizedBox(height: 6,),
                  Padding(
                    padding:EdgeInsets.symmetric(horizontal:6.0),
                    child:Container(
                      height:1.0,
                      width:double.infinity,
                      color:Colors.black,),),
                  Container(
                      margin: EdgeInsets.only(left: 16, right: 16),
                      child: GetBuilder<TagihanController>(
                        init: widget.controller,
                        builder: (params){
                            if(!params.reload_tagihan){
                              return ListView.builder(
                                  itemCount: TransaksiModels.data.length,
                                  shrinkWrap: true,
                                  itemBuilder: (context, item){
                                    return  Column(
                                      mainAxisAlignment: MainAxisAlignment.center,
                                      children: <Widget>[
                                        Container(
                                          margin: EdgeInsets.only(top: 3),
                                          child: Row(
                                            children: <Widget>[
                                              Text("${TransaksiModels.data[item].berat_produk} Kg", textAlign: TextAlign.center,),
                                              Spacer(),
                                              Text("${TransaksiModels.data[item].jenis_produk}", textAlign: TextAlign.justify,),
                                              Spacer(),
                                              Text("${TransaksiModels.data[item].harga}", textAlign: TextAlign.justify,),
                                              Spacer(),
                                              Text("${TransaksiModels.data[item].total_harga}", style: TextStyle(fontWeight: FontWeight.bold), textAlign: TextAlign.justify,),
                                            ],
                                            crossAxisAlignment: CrossAxisAlignment.start,
                                          ),
                                        ),
                                        Container(
                                          child: Text("${TransaksiModels.data[item].keterangan}", textAlign: TextAlign.right,),
                                          alignment: Alignment.topRight,
                                        ),
                                        Container(
                                          alignment: Alignment.centerRight,
                                          margin: EdgeInsets.only(right: 16),
                                          child: GestureDetector(
                                            child: Icon(Icons.delete, color: Colors.pinkAccent,),
                                            onTap: () async{
                                              widget.controller.deleteTransaksiItem(TransaksiModels.data[item].id);
                                            },
                                          ),
                                        ),
                                      ],
                                    );
                                  }
                              );
                            }

                            return CircularProgressIndicator();

                        },
                      )
                  ),
                  Container(
                      margin: EdgeInsets.only(left: 16, right: 16),
                      child: GetBuilder<TagihanController>(
                        init: widget.controller,
                        builder: (params){
                          if(!params.reload_tagihan){
                            return ListView.builder(
                                itemCount: TransaksiModels.data_porsi.length,
                                shrinkWrap: true,
                                itemBuilder: (context, item){
                                  return  Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: <Widget>[
                                      Container(
                                        child: Row(
                                          children: <Widget>[
                                            Text("${TransaksiModels.data_porsi[item].jml_org}"),
                                            Spacer(),
                                            Text("Porsi"),
                                            Spacer(),
                                            Text("10000"),
                                            Spacer(),
                                            Text("${(int.parse(TransaksiModels.data_porsi[item].jml_org.toString()) * 10000).toString()}", style: TextStyle(fontWeight: FontWeight.bold),),
                                          ],
                                          crossAxisAlignment: CrossAxisAlignment.start,
                                        ),
                                      ),
                                      Container(
                                        alignment: Alignment.centerRight,
                                        margin: EdgeInsets.only(right: 16),
                                        child: GestureDetector(
                                          child: Icon(Icons.edit, color: Colors.pinkAccent,),
                                          onTap: () async{
                                            //EDIT JUMLAH ORANG MAKAN
                                            KasirModel.qty_org = TransaksiModels.data_porsi[item].jml_org;
                                            String msg = await Get.to(FormEditOrgMakanView(controller: widget.controller,));
                                            if(msg == "reload"){
                                              widget.controller.getListPesanan();
                                              Get.snackbar("Informasi", "Data Berhasil Diubah");
                                            }
                                          },
                                        ),
                                      ),
                                    ],
                                  );
                                }
                            );
                          }

                          return CircularProgressIndicator();

                        },
                      )
                  ),
                  Container(
                      margin: EdgeInsets.only(left: 16, right: 16),
                      child: GetBuilder<TagihanController>(
                        init: widget.controller,
                        builder: (params){
                          if(!params.reload_tagihan){
                            return ListView.builder(
                                itemCount: TransaksiModels.data_lain.length,
                                shrinkWrap: true,
                                itemBuilder: (context, item){
                                  return  Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: <Widget>[
                                      Container(
                                        child: Row(
                                          children: <Widget>[
                                            Text("${TransaksiModels.data_lain[item].qty}"),
                                            Spacer(),
                                            Text("${TransaksiModels.data_lain[item].menu}"),
                                            Spacer(),
                                            Text("${TransaksiModels.data_lain[item].harga}"),
                                            Spacer(),
                                            Text("${TransaksiModels.data_lain[item].total_harga}", style: TextStyle(fontWeight: FontWeight.bold),),
                                          ],
                                          crossAxisAlignment: CrossAxisAlignment.start,
                                        ),
                                      ),
                                      Container(
                                        alignment: Alignment.centerRight,
                                        margin: EdgeInsets.only(right: 16),
                                        child: GestureDetector(
                                          child: Icon(Icons.delete, color: Colors.pinkAccent,),
                                          onTap: () async{
                                            widget.controller.deleteTransaksiItemLain(TransaksiModels.data_lain[item].id);
                                          },
                                        ),
                                      ),
                                    ],
                                  );
                                }
                            );
                          }

                          return CircularProgressIndicator();

                        },
                      )
                  ),
                  SizedBox(height: 8,),
                  Padding(
                    padding:EdgeInsets.symmetric(horizontal:6.0),
                    child:Container(
                      height:1.0,
                      width:double.infinity,
                      color:Colors.black,),),
                  SizedBox(height: 12,),
                  Container(
                    child: Row(
                      children: <Widget>[
                        Text("", style: TextStyle(fontWeight: FontWeight.bold),),
                        Spacer(),
                        Text("", style: TextStyle(fontWeight: FontWeight.bold)),
                        Spacer(),
                        Text("Total : ", style: TextStyle(fontWeight: FontWeight.bold)),
                        Spacer(),
                        GetBuilder<TagihanController>(
                          init: widget.controller,
                          builder:(params){
                            return Text("Rp, ${params.total_tagihan.toString()}", style: TextStyle(fontWeight: FontWeight.bold));
                          }
                        )
                      ],
                    ),
                  ),
                  Container(
                    child: Row(
                      children: <Widget>[
                        Text("", style: TextStyle(fontWeight: FontWeight.bold),),
                        Spacer(),
                        Text("", style: TextStyle(fontWeight: FontWeight.bold)),
                        Spacer(),
                        Text("Bayar : ", style: TextStyle(fontWeight: FontWeight.bold)),
                        Spacer(),
                        GetBuilder<TagihanController>(
                          init: widget.controller,
                          builder:(params){
                            return Text("Rp, ${params.uang_bayar.toString()}", style: TextStyle(fontWeight: FontWeight.bold));
                          }
                        )
                      ],
                    ),
                  ),
                  Container(
                    child: Row(
                      children: <Widget>[
                        Text("", style: TextStyle(fontWeight: FontWeight.bold),),
                        Spacer(),
                        Text("", style: TextStyle(fontWeight: FontWeight.bold)),
                        Spacer(),
                        Text(" ", style: TextStyle(fontWeight: FontWeight.bold)),
                        Spacer(),
                        GetBuilder<TagihanController>(
                          init: widget.controller,
                          builder:(params){
                            return Text("Rp, ${params.kembalian.toString()}", style: TextStyle(fontWeight: FontWeight.bold));
                          }
                        )
                      ],
                    ),
                  ),

                  SizedBox(height: 16,),
                 GetBuilder<TagihanController>(
                   init: widget.controller,
                   builder: (params){
                     if(params.paid == "1"){
                       return Text("SUDAH TERBAYAR");
                     }

                     return  Container(
                       width: double.infinity,
                       height: 50,
                       child: RaisedButton(
                         child: Text("TAMBAH MINUMAN", style: TextStyle(color: Colors.white),),
                         color: Colors.pinkAccent,
                         onPressed: () async{
                           if(widget.controller.paid == "1"){
                             Get.snackbar("Informasi", "Tidak dapat menambahkan menu, Tagihan Sudah Terbayar");
                           }else{
                             String result = await Get.to(FormAddPesananView(controller: widget.controller,));
                             if(result == "reload"){
                               widget.controller.reload_tagihan = true;
                               widget.controller.update();
                               widget.controller.getListPesanan();
                               Get.snackbar("Informasi", "Menu Lain Ditambahkan");
//                widget.controller.getListPesananLain();
                             }
                           }
//                           KasirModel.qty_org = "";
//                           String result = await Get.to(MainKasirView(
//                             no_nota: KasirModel.no_nota,
//                             addingmenu: false,
//                           ));
//                           if(result == "reload"){
//                             widget.controller.reload_tagihan = true;
//                             widget.controller.update();
//                             widget.controller.getListPesanan();
//                           }
                         },
                         elevation: 3,
                         shape: RoundedRectangleBorder(
                           borderRadius: BorderRadius.circular(8.0),
                         ),
                       ),
                     );
                   }
                 ),
                  SizedBox(height: 8,),
                  GetBuilder<TagihanController>(
                    init: widget.controller,
                    builder: (params){
                      if(params.paid == "1"){
                        return Text("-");
                      }

                      return Container(
                        width: double.infinity,
                        height: 50,
                        child: RaisedButton(
                          child: Text("BAYAR", style: TextStyle(color: Colors.white),),
                          color: Colors.green,
                          onPressed: () async{
                            String result = await Get.to(FormBayarTagihanView(total_tagihan: widget.controller.total_tagihan,));
                            if(result == "reload"){
                              widget.controller.reload_tagihan = true;
                              widget.controller.update();
                              await widget.controller.getListPesanan();
                              Get.snackbar("Informasi", "Tagihan Berhasil Dibayar tunggu device sedang melakukan printing struk", backgroundColor: Colors.green);


                                int total_menu_lain = 0;
                                if(TransaksiModels.data_lain.length > 0){
                                  for(int i = 0; i < TransaksiModels.data_lain.length; i++){
                                    int total = int.parse(TransaksiModels.data_lain[i].total_harga.toString());
                                    total_menu_lain += total;
                                  }
                                }

                                int total_all_tagihan = params.total_tagihan + total_menu_lain;
//                                print(total_all_tagihan.toString());
//                                print("TOTAL LAIN "+total_menu_lain.toString());
//                              //print Struk
//                              //code to print with this device
                              printerManager.selectPrinter(controller_print.devices[0]);
                              Ticket ticket=Ticket(PaperSize.mm58);

                              if(TransaksiModels.data.length > 0) {
                                ticket.row([
                                  PosColumn(
                                    text: TransaksiModels.data[TransaksiModels.data.length - 1].createddate,
                                    width: 12,
                                    styles: PosStyles(
                                        align: PosTextAlign.right, underline: true),
                                  ),
                                ]);
                              }

                              ticket.text('LESEHAN SANTONG', styles: PosStyles(
                                  align: PosTextAlign.center, underline: true));
                              ticket.text('+6285205022764', styles: PosStyles(
                                  align: PosTextAlign.center, underline: true));
                              ticket.text('ANTRIAN : ${KasirModel.no_antrian}', styles: PosStyles(
                                  align: PosTextAlign.center, underline: true));

                              ticket.text("");

                              if(TransaksiModels.data.length > 0){
                                for(int i = 0; i < TransaksiModels.data.length; i++){
                                  ticket.row([
                                    PosColumn(
                                      text: TransaksiModels.data[i].berat_produk+" Kg",
                                      width: 3,
                                      styles: PosStyles(align: PosTextAlign.center, underline: true),
                                    ),
                                    PosColumn(
                                      text: TransaksiModels.data[i].jenis_produk,
                                      width: 3,
                                      styles: PosStyles(align: PosTextAlign.center, underline: true),
                                    ),
                                    PosColumn(
                                      text: TransaksiModels.data[i].harga,
                                      width: 3,
                                      styles: PosStyles(align: PosTextAlign.center, underline: true),
                                    ),
                                    PosColumn(
                                      text: TransaksiModels.data[i].total_harga,
                                      width: 3,
                                      styles: PosStyles(align: PosTextAlign.center, underline: true),
                                    ),
                                  ]);
                                }
                              }


                              if(TransaksiModels.data_porsi.length > 0){
                                for(int i = 0; i < TransaksiModels.data_porsi.length; i++){
                                  ticket.row([
                                    PosColumn(
                                      text: TransaksiModels.data_porsi[i].jml_org,
                                      width: 3,
                                      styles: PosStyles(align: PosTextAlign.center, underline: true),
                                    ),
                                    PosColumn(
                                      text: "Porsi",
                                      width: 3,
                                      styles: PosStyles(align: PosTextAlign.center, underline: true),
                                    ),
                                    PosColumn(
                                      text: "10000",
                                      width: 3,
                                      styles: PosStyles(align: PosTextAlign.center, underline: true),
                                    ),
                                    PosColumn(
                                      text: (int.parse(TransaksiModels.data_porsi[i].jml_org.toString()) * 10000).toString(),
                                      width: 3,
                                      styles: PosStyles(align: PosTextAlign.center, underline: true),
                                    ),
                                  ]);
                                }
                              }


                              if(TransaksiModels.data_lain.length > 0){
                                for(int i = 0; i < TransaksiModels.data_lain.length; i++){
                                  ticket.row([
                                    PosColumn(
                                      text: TransaksiModels.data_lain[i].qty+"",
                                      width: 3,
                                      styles: PosStyles(align: PosTextAlign.center, underline: true),
                                    ),
                                    PosColumn(
                                      text: TransaksiModels.data_lain[i].menu,
                                      width: 3,
                                      styles: PosStyles(align: PosTextAlign.center, underline: true),
                                    ),
                                    PosColumn(
                                      text: TransaksiModels.data_lain[i].harga,
                                      width: 3,
                                      styles: PosStyles(align: PosTextAlign.center, underline: true),
                                    ),
                                    PosColumn(
                                      text: TransaksiModels.data_lain[i].total_harga,
                                      width: 3,
                                      styles: PosStyles(align: PosTextAlign.center, underline: true),
                                    ),
                                  ]);
                                }
                              }

                              ticket.text("");

                              ticket.row([
                                PosColumn(
                                  text: "",
                                  width: 3,
                                  styles: PosStyles(align: PosTextAlign.center, underline: true),
                                ),
                                PosColumn(
                                  text: "",
                                  width: 3,
                                  styles: PosStyles(align: PosTextAlign.center, underline: true),
                                ),
                                PosColumn(
                                  text: "Total : ",
                                  width: 3,
                                  styles: PosStyles(align: PosTextAlign.center, underline: true),
                                ),
                                PosColumn(
                                  text: total_all_tagihan.toString(),
                                  width: 3,
                                  styles: PosStyles(align: PosTextAlign.center, underline: true),
                                ),
                              ]);

                              ticket.row([
                                PosColumn(
                                  text: "",
                                  width: 3,
                                  styles: PosStyles(align: PosTextAlign.center, underline: true),
                                ),
                                PosColumn(
                                  text: "",
                                  width: 3,
                                  styles: PosStyles(align: PosTextAlign.center, underline: true),
                                ),
                                PosColumn(
                                  text: "Uang Bayar : ",
                                  width: 3,
                                  styles: PosStyles(align: PosTextAlign.center, underline: true),
                                ),
                                PosColumn(
                                  text: params.uang_bayar.toString(),
                                  width: 3,
                                  styles: PosStyles(align: PosTextAlign.center, underline: true),
                                ),
                              ]);

                              ticket.row([
                                PosColumn(
                                  text: "",
                                  width: 3,
                                  styles: PosStyles(align: PosTextAlign.center, underline: true),
                                ),
                                PosColumn(
                                  text: "",
                                  width: 3,
                                  styles: PosStyles(align: PosTextAlign.center, underline: true),
                                ),
                                PosColumn(
                                  text: "Kembalian : ",
                                  width: 3,
                                  styles: PosStyles(align: PosTextAlign.center, underline: true),
                                ),
                                PosColumn(
                                  text: params.kembalian.toString(),
                                  width: 3,
                                  styles: PosStyles(align: PosTextAlign.center, underline: true),
                                ),
                              ]);

                              ticket.row([
                                PosColumn(
                                  text: "Terima Kasih",
                                  width: 12,
                                  styles: PosStyles(align: PosTextAlign.center, underline: true),
                                ),
                              ]);


                              ticket.feed(1);
                              ticket.cut();

                              printerManager.printTicket(ticket).then((result) {
                                Get.snackbar("Informasi", result.msg +" printing", backgroundColor: Colors.green);
                              }).catchError((error){
                                Get.snackbar("Informasi", error.toString(), backgroundColor: Colors.red);
                              });
                            }
                          },
                          elevation: 3,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(8.0),
                          ),
                        ),
                      );
                    }
                  ),
                  SizedBox(height: 8,),
                  Container(
                    width: double.infinity,
                    height: 50,
                    child: RaisedButton(
                      child: Text("PRINT STRUK", style: TextStyle(color: Colors.black87),),
                      color: Colors.white,
                      onPressed: (){
                        Get.to(PrintingView(
                          data_trans: TransaksiModels.data,
                          data_lain: TransaksiModels.data_lain,
                          controller: widget.controller,
                          data_porsi: TransaksiModels.data_porsi,));
                      },
                      elevation: 3,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(8.0),
                      ),
                    ),
                  ),
                ],
              ),
            )
          ],
          footer: Footer(
            backgroundColor: Colors.white,
            child: FooterDashboardView(),
          ),
        ),
//        floatingActionButton: FloatingActionButton(
//            backgroundColor: Colors.pinkAccent,
//            child: Icon(Icons.add),
//            onPressed: () async{
//              if(widget.controller.paid == "1"){
//                Get.snackbar("Informasi", "Tidak dapat menambahkan menu, Tagihan Sudah Terbayar");
//              }else{
//                String result = await Get.to(FormAddPesananView(controller: widget.controller,));
//                if(result == "reload"){
//                  widget.controller.reload_tagihan = true;
//                  widget.controller.update();
//                  widget.controller.getListPesanan();
//                  Get.snackbar("Informasi", "Menu Lain Ditambahkan");
////                widget.controller.getListPesananLain();
//                }
//              }
//            },
//        )
    );
  }
}
