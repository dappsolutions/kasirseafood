import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:wponline/app/controllers/formbayar.controllers.dart';
import 'package:wponline/app/models/kasir.models.dart';
import 'package:wponline/app/models/tagihan.models.dart';
import 'package:wponline/resources/views/dashboard/title.views.dart';
import 'package:wponline/resources/views/kasir/titleform.kasir.views.dart';


class FormBayarTagihanView extends StatefulWidget {
  int total_tagihan = 0;
  FormBayarTagihanView({this.total_tagihan});

  @override
  _FormBayarTagihanViewState createState() => _FormBayarTagihanViewState();
}

class _FormBayarTagihanViewState extends State<FormBayarTagihanView> {
  FormBayarController controller;


  @override
  void initState() {
    controller = new FormBayarController();
    TagihanModels.uang_bayar = "";
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: SingleChildScrollView(
          child: Container(
              child: Column(
                children: <Widget>[
                  TitleView(title: "KASIR DAPPSPOS",),
                  TitleFormView(title: "BAYAR TAGIHAN"),
                  Container(
                      padding: EdgeInsets.only(left: 32, right: 32, top: 24),
                      child: TextFormField(
                        decoration: InputDecoration(
                            prefixIcon: Icon(Icons.filter_none),
                            border: OutlineInputBorder(),
                            hintText: "NOMOR TRANSAKSI",
                            labelText: "NOMOR TRANSAKSI"
                        ),
                        enabled: false,
                        initialValue: KasirModel.no_nota,
                        keyboardType: TextInputType.text,
                        validator: (text){
                          if(text.isEmpty){
                            return "NOMOR ANTRIAN Wajib Diisi";
                          }
                          return null;
                        },
                      )
                  ),
                  Container(
                      padding: EdgeInsets.only(left: 32, right: 32, top: 24),
                      child: TextFormField(
                        decoration: InputDecoration(
                            prefixIcon: Icon(Icons.filter_none),
                            border: OutlineInputBorder(),
                            hintText: "NOMOR ANTRIAN",
                            labelText: "NOMOR ANTRIAN"
                        ),
                        enabled: false,
                        initialValue: KasirModel.no_antrian,
                        keyboardType: TextInputType.text,
                        validator: (text){
                          if(text.isEmpty){
                            return "NOMOR ANTRIAN Wajib Diisi";
                          }
                          return null;
                        },
                      )
                  ),
                  Container(
                      padding: EdgeInsets.only(left: 32, right: 32, top: 16),
                      child: TextFormField(
                        decoration: InputDecoration(
                            prefixIcon: Icon(Icons.attach_money),
                            hintText: "TOTAL TAGIHAN",
                            labelText: "TOTAL TAGIHAN"
                        ),
                        keyboardType: TextInputType.number,
                        initialValue: widget.total_tagihan.toString(),
                        enabled: false,
                        validator: (text){
                          if(text.isEmpty){
                            return "TOTAL TAGIHAN Wajib Diisi";
                          }
                          return null;
                        },
                        onChanged: (val){
                          TagihanModels.uang_bayar = val.toString();
                        },
                      )
                  ),
                  Container(
                      padding: EdgeInsets.only(left: 32, right: 32, top: 16),
                      child: TextFormField(
                        decoration: InputDecoration(
                            prefixIcon: Icon(Icons.attach_money),
                            hintText: "UANG BAYAR",
                            labelText: "UANG BAYAR"
                        ),
                        keyboardType: TextInputType.number,
                        validator: (text){
                          if(text.isEmpty){
                            return "UANG BAYAR Wajib Diisi";
                          }
                          return null;
                        },
                        onChanged: (val){
                          TagihanModels.uang_bayar = val.toString();
                        },
                      )
                  ),

                  Container(
                    padding: EdgeInsets.only(left: 32, right: 32, top: 16),
                    width: double.infinity,
                    height: 70,
                    child: RaisedButton(
                      child: Text("PROSES", style: TextStyle(color: Colors.white),),
                      color: Colors.green,
                      onPressed: () async{
//                        Get.back(result: "reload");
                        String message = await controller.prosesBayar(widget.total_tagihan);
                        if(message == "success"){
                            Get.back(result: "reload");
                        }

                      },
                      elevation: 8,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(8.0),
                      ),
                    ),

                  ),
                ],
              )
          ),
        )
    );
  }
}
