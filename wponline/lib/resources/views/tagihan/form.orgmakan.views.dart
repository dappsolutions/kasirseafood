import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:wponline/app/controllers/tagihan.controllers.dart';
import 'package:wponline/app/models/kasir.models.dart';
import 'package:wponline/resources/views/dashboard/title.views.dart';
import 'package:wponline/resources/views/kasir/titleform.kasir.views.dart';

class FormEditOrgMakanView extends StatefulWidget {
  TagihanController controller;
  FormEditOrgMakanView({this.controller});

  @override
  _FormEditOrgMakanViewState createState() => _FormEditOrgMakanViewState();
}

class _FormEditOrgMakanViewState extends State<FormEditOrgMakanView> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: SingleChildScrollView(
          child: Container(
              child: Column(
                children: <Widget>[
                  TitleView(title: "KASIR DAPPSPOS",),
                  TitleFormView(title: "EDIT JUMLAH \nORANG MAKAN"),
                  Container(
                      padding: EdgeInsets.only(left: 32, right: 32, top: 24),
                      child: TextFormField(
                        decoration: InputDecoration(
                            prefixIcon: Icon(Icons.filter_none),
                            border: OutlineInputBorder(),
                            hintText: "NOMOR TRANSAKSI",
                            labelText: "NOMOR TRANSAKSI"
                        ),
                        enabled: false,
                        initialValue: KasirModel.no_nota,
                        keyboardType: TextInputType.text,
                        validator: (text){
                          if(text.isEmpty){
                            return "NOMOR ANTRIAN Wajib Diisi";
                          }
                          return null;
                        },
                      )
                  ),
                  Container(
                      padding: EdgeInsets.only(left: 32, right: 32, top: 24),
                      child: TextFormField(
                        decoration: InputDecoration(
                            prefixIcon: Icon(Icons.filter_none),
                            border: OutlineInputBorder(),
                            hintText: "NOMOR ANTRIAN",
                            labelText: "NOMOR ANTRIAN"
                        ),
                        enabled: false,
                        initialValue: KasirModel.no_antrian,
                        keyboardType: TextInputType.text,
                        validator: (text){
                          if(text.isEmpty){
                            return "NOMOR ANTRIAN Wajib Diisi";
                          }
                          return null;
                        },
                      )
                  ),
                  Container(
                      padding: EdgeInsets.only(left: 32, right: 32, top: 16),
                      child: TextFormField(
                        decoration: InputDecoration(
                            prefixIcon: Icon(Icons.people),
                            hintText: "JUMLAH ORG",
                            labelText: "JUMLAH ORG"
                        ),
                        keyboardType: TextInputType.number,
                        initialValue: KasirModel.qty_org,
                        validator: (text){
                          if(text.isEmpty){
                            return "JUMLAH ORG Wajib Diisi";
                          }
                          return null;
                        },
                        onChanged: (val){
                          KasirModel.qty_org = val.toString();
                        },
                      )
                  ),

                  Container(
                    padding: EdgeInsets.only(left: 32, right: 32, top: 16),
                    width: double.infinity,
                    height: 70,
                    child: RaisedButton(
                      child: Text("PROSES", style: TextStyle(color: Colors.white),),
                      color: Colors.green,
                      onPressed: () async{
                        String message = await widget.controller.editJumlahOrgMakan();
//                        print("MESSAHE ${message}");
                        if(message == "success"){
                            Get.back(result: "reload");
                        }
                      },
                      elevation: 8,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(8.0),
                      ),
                    ),

                  ),
                ],
              )
          ),
        )
    );
  }
}
