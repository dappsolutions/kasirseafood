import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:wponline/app/controllers/addpesanan.controllers.dart';
import 'package:wponline/app/controllers/tagihan.controllers.dart';
import 'package:wponline/app/models/kasir.models.dart';
import 'package:wponline/app/models/tagihan.models.dart';
import 'package:wponline/resources/views/dashboard/title.views.dart';
import 'package:wponline/resources/views/kasir/titleform.kasir.views.dart';


class FormAddPesananView extends StatefulWidget {
  TagihanController controller;
  FormAddPesananView({this.controller});

  @override
  _FormAddPesananViewState createState() => _FormAddPesananViewState();
}

class _FormAddPesananViewState extends State<FormAddPesananView> {

  AddPesananController controller;

  int total = 0;

  @override
  void initState() {
    controller = new AddPesananController();

    widget.controller.total_harga_menu_lain = 0;
    TagihanModels.jumlah = "";
    TagihanModels.harga = "";
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Container(
            child: Column(
              children: <Widget>[
                TitleView(title: "KASIR DAPPSPOS",),
                TitleFormView(title: "PESANAN TAMBAHAN"),
                Container(
                    padding: EdgeInsets.only(left: 32, right: 32, top: 24),
                    child: TextFormField(
                      decoration: InputDecoration(
                          prefixIcon: Icon(Icons.filter_none),
                          border: OutlineInputBorder(),
                          hintText: "NOMOR TRANSAKSI",
                          labelText: "NOMOR TRANSAKSI"
                      ),
                      enabled: false,
                      initialValue: KasirModel.no_nota,
                      keyboardType: TextInputType.text,
                      validator: (text){
                        if(text.isEmpty){
                          return "NOMOR ANTRIAN Wajib Diisi";
                        }
                        return null;
                      },
                    )
                ),
                Container(
                    padding: EdgeInsets.only(left: 32, right: 32, top: 24),
                    child: TextFormField(
                      decoration: InputDecoration(
                          prefixIcon: Icon(Icons.filter_none),
                          border: OutlineInputBorder(),
                          hintText: "NOMOR ANTRIAN",
                          labelText: "NOMOR ANTRIAN"
                      ),
                      enabled: false,
                      initialValue: KasirModel.no_antrian,
                      keyboardType: TextInputType.text,
                      validator: (text){
                        if(text.isEmpty){
                          return "NOMOR ANTRIAN Wajib Diisi";
                        }
                        return null;
                      },
                    )
                ),
                Container(
                    padding: EdgeInsets.only(left: 32, right: 32, top: 16),
                    child: TextFormField(
                      decoration: InputDecoration(
                          prefixIcon: Icon(Icons.filter_none),
                          hintText: "MENU",
                          labelText: "MENU"
                      ),
                      keyboardType: TextInputType.text,
                      validator: (text){
                        if(text.isEmpty){
                          return "MENU Wajib Diisi";
                        }
                        return null;
                      },
                      onChanged: (val){
                        TagihanModels.menu = val.toString();
                      },
                    )
                ),
                Container(
                    padding: EdgeInsets.only(left: 32, right: 32, top: 16),
                    child: TextFormField(
                      decoration: InputDecoration(
                          prefixIcon: Icon(Icons.filter_none),
                          hintText: "JUMLAH",
                          labelText: "JUMLAH"
                      ),
                      keyboardType: TextInputType.number,
                      validator: (text){
                        if(text.isEmpty){
                          return "JUMLAH Wajib Diisi";
                        }
                        return null;
                      },
                      onChanged: (val){
                        TagihanModels.jumlah = val.toString();
                        hitungTotalHargaMenu();
                      },
                    )
                ),
                Container(
                    padding: EdgeInsets.only(left: 32, right: 32, top: 16),
                    child: TextFormField(
                      decoration: InputDecoration(
                          prefixIcon: Icon(Icons.attach_money),
                          hintText: "HARGA",
                          labelText: "HARGA"
                      ),
                      keyboardType: TextInputType.number,
                      validator: (text){
                        if(text.isEmpty){
                          return "HARGA Wajib Diisi";
                        }
                        return null;
                      },
                      onChanged: (val){
                        TagihanModels.harga = val.toString();
                        hitungTotalHargaMenu();
                      },
                    )
                ),
//                Container(
//                    padding: EdgeInsets.only(left: 32, right: 32, top: 24),
//                    child: TextFormField(
//                      decoration: InputDecoration(
//                          prefixIcon: Icon(Icons.filter_none),
//                          border: OutlineInputBorder(),
//                          hintText: "TOTAL",
//                          labelText: "TOTAL"
//                      ),
//                      enabled: false,
//                      initialValue: this.total.toString(),
//                      keyboardType: TextInputType.number,
//                      validator: (text){
//                        if(text.isEmpty){
//                          return "TOTAL Wajib Diisi";
//                        }
//                        return null;
//                      },
//                    )
//                ),
                Container(
                  padding: EdgeInsets.only(left: 32, right: 32, top: 16),
                  width: double.infinity,
                  height: 70,
                  child: RaisedButton(
                    child: Text("SIMPAN", style: TextStyle(color: Colors.white),),
                    color: Colors.pinkAccent,
                    onPressed: () async{
//                      Get.back(result: "reload");
                      String message = await widget.controller.addPesanan();
                      if(message == "success"){
                          Get.back(result: "reload");
                      }
                    },
                    elevation: 8,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(8.0),
                    ),
                  ),

                ),
              ],
            )
        ),
      )
    );
  }


  void hitungTotalHargaMenu() {

    int jumlah = TagihanModels.jumlah == "" ? 0 : int.parse(TagihanModels.jumlah);
    int harga = TagihanModels.harga == "" ? 0 : int.parse(TagihanModels.harga);

    setState(() {
      this.total = jumlah * harga;
    });
  }
}
