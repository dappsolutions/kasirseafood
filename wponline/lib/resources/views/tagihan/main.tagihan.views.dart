import 'package:flutter/material.dart';
import 'package:footer/footer.dart';
import 'package:footer/footer_view.dart';
import 'package:get/get.dart';
import 'package:search_page/search_page.dart';
import 'package:wponline/app/controllers/tagihan.controllers.dart';
import 'package:wponline/app/models/kasir.models.dart';
import 'package:wponline/app/models/tagihan.models.dart';
import 'package:wponline/app/storage/produk_transaksi.dart';
import 'package:wponline/resources/views/dashboard/footer.views.dart';
import 'package:wponline/resources/views/dashboard/title.views.dart';
import 'package:wponline/resources/views/kasir/titleform.kasir.views.dart';
import 'package:wponline/resources/views/tagihan/form.tagihan.views.dart';


class MainTagihanView extends StatefulWidget {
  @override
  _MainTagihanViewState createState() => _MainTagihanViewState();
}

class _MainTagihanViewState extends State<MainTagihanView> {
  TagihanController controller;

  @override
  void initState() {
    controller = new TagihanController();
    controller.getListAntrianTagihan();
    super.initState();
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: FooterView(
        children: <Widget>[
          TitleView(title: "DAPPSPOS",),
          Center(
            child: TitleFormView(title: "ANTRIAN TAGIHAN",),
          ),
          Container(
            margin: EdgeInsets.only(left: 16, right: 16, top: 8),
            width: double.infinity,
            height: 50,
            child: RaisedButton(
              child: Text("TRANSFER TRANSAKSI", style: TextStyle(color: Colors.white),),
              color: Colors.green,
              onPressed: (){
                  controller.generateTransaksi();
              },
              elevation: 3,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(8.0),
              ),
            ),
          ),
          SingleChildScrollView(
            child: Container(
                padding: EdgeInsets.only(left: 32, right: 32),
                child: GetBuilder<TagihanController>(
                    init: controller,
                    builder: (params){
                      if(!params.reload){
                        return ListView.builder(
                          itemCount: TagihanModels.data.length,
                          shrinkWrap: true,
                          itemBuilder: (context, index) {
                            final Produk_transaksi p_trans = TagihanModels.data[index];
                            return ListTile(
                              title: Text(p_trans.no_transaksi+"\nANTRIAN : ${p_trans.no_antrian}", style: TextStyle(fontWeight: FontWeight.bold),),
                              subtitle: Text(p_trans.createddate),
                              trailing: Text('Pilih', style: TextStyle(color: Colors.pinkAccent),),
                              onTap: (){
//                    print("data ${p_trans.no_transaksi}");
                                KasirModel.no_nota = p_trans.no_transaksi;
                                KasirModel.no_antrian = p_trans.no_antrian;
                                KasirModel.qty_org = p_trans.qty_org;
                                Get.to(FormTagihanView(controller: controller,));
                              },
                            );
                          },
                        );
                      }

                      return Container(
                        child: Center(
                          child: CircularProgressIndicator(),
                        ),
                      );
                    }
                )
            )
          )
        ],
        footer: Footer(
          backgroundColor: Colors.white,
          child: FooterDashboardView(),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.search),
        backgroundColor: Colors.pinkAccent,
        tooltip: "Cari No Antrian",
        onPressed: () => showSearch(
          context: context,
          delegate: SearchPage<Produk_transaksi>(
            items: TagihanModels.data,
            searchLabel: 'Cari No Antrian',
            suggestion: Center(
              child: Text('Cari Antrian Berdasarkan No. Antrian'),
            ),
            failure: Center(
              child: Text('Tidak ada antrian ditemukan :('),
            ),
            filter: (data) => [
              data.no_transaksi,
              data.no_antrian,
              data.createddate,
            ],
            builder: (data) => ListTile(
              title: Text(data.no_transaksi+"\nANTRIAN : ${data.no_antrian}", style: TextStyle(fontWeight: FontWeight.bold),),
              subtitle: Text(data.createddate),
              trailing: Text('Pilih', style: TextStyle(color: Colors.pinkAccent),),
              onTap: (){
                KasirModel.no_nota = data.no_transaksi;
                KasirModel.no_antrian = data.no_antrian;
                KasirModel.qty_org = data.qty_org;
                Get.to(FormTagihanView(controller: controller,));
              },
            ),
          ),
        ),
      ),
    );
  }
}
