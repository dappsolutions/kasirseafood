

import 'package:wponline/app/config/url.dart';
import 'package:wponline/app/models/kasir.models.dart';
import 'package:wponline/app/models/login.models.dart';
import 'package:wponline/app/models/menu.models.dart';
import 'package:wponline/app/models/produk.models.dart';

class Api{
  static Map route ={
    KasirModel.modules :{
      'generateNoTransaksi': Url.appsurl+"/"+KasirModel.modules+"/generateNoTransaksi",
      'simpan' : Url.appsurl+"/"+KasirModel.modules+"/simpan",
      'selesaiProses' : Url.appsurl+"/"+KasirModel.modules+"/selesaiProses",
      'addPesanan' : Url.appsurl+"/"+KasirModel.modules+"/addPesanan",
      'generateTransaksi' : Url.appsurl+"/"+KasirModel.modules+"/generateTransaksi",
      'getListAntrianTagihan' : Url.appsurl+"/"+KasirModel.modules+"/getListAntrianTagihan",
      'getListLaporan' : Url.appsurl+"/"+KasirModel.modules+"/getListLaporan",
      'getLaporanStok' : Url.appsurl+"/"+KasirModel.modules+"/getLaporanStok",
      'getListLaporanBulanan' : Url.appsurl+"/"+KasirModel.modules+"/getListLaporanBulanan",
      'getListPesanan': (String no_nota){
        return Url.appsurl+"/"+KasirModel.modules+"/getListPesanan/${no_nota}";
      },
      'getListPorsiOrg': (String no_nota){
        return Url.appsurl+"/"+KasirModel.modules+"/getListPorsiOrg/${no_nota}";
      },
      'getListPesananByNoTambahan': (String no_nota, String no_tambahan){
        return Url.appsurl+"/"+KasirModel.modules+"/getListPesananByNoTambahan/${no_nota}/${no_tambahan}";
      },
      'getListPesananLain': (String no_nota){
        return Url.appsurl+"/"+KasirModel.modules+"/getListPesananLain/${no_nota}";
      },
      'generateNoTambahan': (String no_nota){
        return Url.appsurl+"/"+KasirModel.modules+"/generateNoTambahan/${no_nota}";
      },
      'deleteLaporan' : Url.appsurl+"/"+KasirModel.modules+"/deleteLaporan",
      'deleteAllLaporan' : Url.appsurl+"/"+KasirModel.modules+"/deleteAllLaporan",
      'deleteTransaksi' : Url.appsurl+"/"+KasirModel.modules+"/deleteTransaksi",
      'deleteTransaksiItem' : Url.appsurl+"/"+KasirModel.modules+"/deleteTransaksiItem",
      'deleteTransaksiItemLain' : Url.appsurl+"/"+KasirModel.modules+"/deleteTransaksiItemLain",
      'prosesBayar' : Url.appsurl+"/"+KasirModel.modules+"/prosesBayar",
      'editJumlahOrgMakan' : Url.appsurl+"/"+KasirModel.modules+"/editJumlahOrgMakan",
    },
    ProdukModel.modules :{
      'getListProduk': Url.appsurl+"/"+ProdukModel.modules+"/getListProduk",
      'getListStok': Url.appsurl+"/"+ProdukModel.modules+"/getListStok",
      'getLaporanPenambahanStok': Url.appsurl+"/"+ProdukModel.modules+"/getLaporanPenambahanStok",
      'simpan': Url.appsurl+"/"+ProdukModel.modules+"/simpan",
      'simpanStok': Url.appsurl+"/"+ProdukModel.modules+"/simpanStok",
      'deleteLaporanStok': Url.appsurl+"/"+ProdukModel.modules+"/deleteLaporanStok",
      'deleteData': Url.appsurl+"/"+ProdukModel.modules+"/deleteData",
    },
    MenuModel.modules :{
      'getListMenu': Url.appsurl+"/"+MenuModel.modules+"/getListMenu",
      'simpan': Url.appsurl+"/"+MenuModel.modules+"/simpan",
      'deleteData': Url.appsurl+"/"+MenuModel.modules+"/deleteData"
    },
    LoginModel.modules:{
      "sign_in": Url.appsurl+"/"+LoginModel.modules+"/sign_in"
    }
  };
}