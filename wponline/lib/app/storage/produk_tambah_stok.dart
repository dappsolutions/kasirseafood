

class Produk_tambah_stok{
  String id;
  String nama_produk;
  String supplier;
  String stok;
  String createddate;
  Produk_tambah_stok({this.id, this.nama_produk, this.supplier, this.stok, this.createddate});


  Produk_tambah_stok.fromJson(Map<String, dynamic> json){
    this.id = json['id'].toString();
    this.nama_produk = json['nama_produk'];
    this.supplier = json['supplier'];
    this.stok = json['stok'].toString();
    this.createddate = json['createddate'];
  }

  Map<String, dynamic> toJson(){
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['nama_produk'] = this.nama_produk;
    data['supplier'] = this.supplier;
    data['stok'] = this.stok;
    data['createddate'] = this.createddate;

    return data;
  }
}