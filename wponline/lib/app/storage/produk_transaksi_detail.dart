

class Produk_transaksi_detail{
  String id;
  String qty_org;
  String qty_produk;
  String berat_produk;
  String jenis_produk;
  String jenis_menu;
  String total_harga;
  String keterangan;
  String harga;
  String createddate;
  Produk_transaksi_detail({this.id, this.qty_org, this.qty_produk, this.berat_produk,
    this.jenis_produk, this.total_harga, this.jenis_menu, this.keterangan,
    this.harga, this.createddate});


  Produk_transaksi_detail.fromJson(Map<String, dynamic> json){
    this.id = json['id'];
    this.qty_org = json['qty_org'];
    this.qty_produk = json['qty_produk'];
    this.berat_produk = json['berat_produk'];
    this.jenis_produk = json['jenis_produk'];
    this.jenis_menu = json['jenis_menu'];
    this.total_harga = json['total_harga'];
    this.keterangan = json['keterangan'];
    this.harga = json['harga'].toString();
    this.createddate = json['createddate'];
  }

  Map<String, dynamic> toJson(){
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['qty_org'] = this.qty_org;
    data['qty_produk'] = this.qty_produk;
    data['berat_produk'] = this.berat_produk;
    data['jenis_produk'] = this.jenis_produk;
    data['jenis_menu'] = this.jenis_menu;
    data['total_harga'] = this.total_harga;
    data['keterangan'] = this.keterangan;
    data['harga'] = this.harga;
    data['createddate'] = this.createddate;

    return data;
  }
}