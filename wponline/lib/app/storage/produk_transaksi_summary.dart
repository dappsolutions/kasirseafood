

class Produk_transaksi_summary{
  String id;
  String no_transaksi;
  String total;
  String createddate;
  String no_antrian;
  Produk_transaksi_summary({this.id, this.no_transaksi,this.total, this.createddate, this.no_antrian});


  Produk_transaksi_summary.fromJson(Map<String, dynamic> json){
    this.id = json['id'];
    this.no_transaksi = json['no_transaksi'];
    this.total = json['total'];
    this.createddate = json['createddate'];
    this.no_antrian = json['no_antrian'].toString();
  }

  Map<String, dynamic> toJson(){
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['no_transaksi'] = this.no_transaksi;
    data['total'] = this.total;
    data['createddate'] = this.createddate;
    data['no_antrian'] = this.no_antrian;

    return data;
  }
}