

class Produk_transaksi{
  String id;
  String no_transaksi;
  String createddate;
  String no_antrian;
  String qty_org;
  Produk_transaksi({this.id, this.no_transaksi, this.createddate, this.no_antrian, this.qty_org});


  Produk_transaksi.fromJson(Map<String, dynamic> json){
    this.id = json['id'];
    this.no_transaksi = json['no_transaksi'];
    this.createddate = json['createddate'];
    this.no_antrian = json['no_antrian'].toString();
    this.qty_org = json['qty_org'].toString();
  }

  Map<String, dynamic> toJson(){
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['no_transaksi'] = this.no_transaksi;
    data['createddate'] = this.createddate;
    data['no_antrian'] = this.no_antrian;
    data['this.qty_org'] = this.qty_org;

    return data;
  }
}