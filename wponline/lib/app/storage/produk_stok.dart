

class Produk_stok{
  String id;
  String total_qty_produk;
  String total_berat_produk;
  String harga_per_kg;
  String total_harga;
  String menu;
  Produk_stok({this.id, this.total_qty_produk, this.total_berat_produk, this.harga_per_kg, this.total_harga, this.menu});


  Produk_stok.fromJson(Map<String, dynamic> json){
    this.id = json['id'].toString();
    this.total_qty_produk = json['total_qty_produk'].toString();
    this.total_berat_produk = json['total_berat_produk'].toString();
    this.harga_per_kg = json['harga_per_kg'].toString();
    this.total_harga = json['total_harga'].toString();
    this.menu = json['menu'].toString();
  }

  Map<String, dynamic> toJson(){
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['total_qty_produk'] = this.total_qty_produk;
    data['total_berat_produk'] = this.total_berat_produk;
    data['harga_per_kg'] = this.harga_per_kg;
    data['total_harga'] = this.total_harga;
    data['menu'] = this.menu;

    return data;
  }
}