

class Produk{
  String id;
  String jenis;
  Produk({this.id, this.jenis});


  Produk.fromJson(Map<String, dynamic> json){
    this.id = json['id'];
    this.jenis = json['jenis'];
  }

  Map<String, dynamic> toJson(){
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['jenis'] = this.jenis;

    return data;
  }
}