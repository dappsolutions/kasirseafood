

class Produk_transaksi_lain{
  String id;
  String menu;
  String harga;
  String qty;
  String total_harga;
  Produk_transaksi_lain({this.id, this.menu, this.harga, this.qty, this.total_harga});


  Produk_transaksi_lain.fromJson(Map<String, dynamic> json){
    this.id = json['id'];
    this.menu = json['menu'];
    this.harga = json['harga'];
    this.qty = json['qty'];
    this.total_harga = json['total_harga'];
  }

  Map<String, dynamic> toJson(){
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['menu'] = this.menu;
    data['harga'] = this.harga;
    data['qty'] = this.qty;
    data['total_harga'] = this.total_harga;

    return data;
  }
}