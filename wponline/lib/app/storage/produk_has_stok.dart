

class Produk_has_stok{
  String id;
  String jenis;
  String stok_awal;
  String stok_keluar;
  String stok_outstanding;
  String harga;
  Produk_has_stok({this.id, this.jenis, this.stok_awal, this.stok_keluar, this.stok_outstanding, this.harga});


  Produk_has_stok.fromJson(Map<String, dynamic> json){
    this.id = json['id'];
    this.jenis = json['jenis'];
    this.stok_awal = json['stok_awal'].toString();
    this.stok_keluar = json['stok_keluar'].toString();
    this.stok_outstanding = json['stok_outstanding'].toString();
    this.harga = json['harga'].toString();
  }

  Map<String, dynamic> toJson(){
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['jenis'] = this.jenis;
    data['stok_awal'] = this.stok_awal;
    data['stok_keluar'] = this.stok_keluar;
    data['stok_outstanding'] = this.stok_outstanding;
    data['harga'] = this.harga;

    return data;
  }
}