
import 'package:esc_pos_printer/esc_pos_printer.dart';
import 'package:get/get.dart';
import 'package:http/http.dart' as http;
import 'package:wponline/app/models/kasir.models.dart';
import 'package:wponline/app/models/tagihan.models.dart';
import 'package:wponline/app/models/transaksi.models.dart';
import 'package:wponline/app/storage/porsi.dart';
import 'package:wponline/app/storage/produk_transaksi.dart';
import 'package:wponline/app/storage/produk_transaksi_detail.dart';
import 'package:wponline/app/storage/produk_transaksi_lain.php.dart';
import 'dart:convert';

import 'package:wponline/routes/api.dart';

class TagihanController extends GetxController{
  bool reload = true;
  bool reload_tagihan = true;
  int total_tagihan = 0;
  int total_porsi = 0;
  int total_harga_menu_lain = 0;

  int uang_bayar = 0;
  int kembalian = 0;
  String paid = "0";


  bool scanning = false;
  List<PrinterBluetooth> devices = [];


  void getListAntrianTagihan() async{
    var request = await http
        .get(Api.route[KasirModel.modules]['getListAntrianTagihan']);

    if (request.statusCode == 200) {
      var data = json.decode(request.body);
      List<Produk_transaksi> dataAdd = [];
      for (var item in data['data']) {
        item["id"] = item['id'].toString();
        dataAdd.add(Produk_transaksi.fromJson(item));
      }

      TagihanModels.data.clear();
      TagihanModels.data.addAll(dataAdd);
      this.reload = false;
      update();
    }
  }

  void getListPesanan() async{
    String no_nota = KasirModel.no_nota;
    var request = await http
        .get(Api.route[KasirModel.modules]['getListPesanan'](no_nota));

    if (request.statusCode == 200) {
      int total_tagih = 0;
      var data = json.decode(request.body);
      List<Produk_transaksi_detail> dataAdd = [];
      for (var item in data['data']) {
        item["id"] = item['id'].toString();
        item["qty_org"] = item['qty_org'].toString();
        item["qty_produk"] = item['qty_produk'].toString();
        item["berat_produk"] = item['berat_produk'].toString();
        item["total_harga"] = item['total_harga'].toString();
        total_tagih += int.parse(item['total_harga'].toString());
        dataAdd.add(Produk_transaksi_detail.fromJson(item));
      }

      TransaksiModels.data.clear();
      TransaksiModels.data.addAll(dataAdd);
      this.reload_tagihan = false;
      this.total_tagihan = total_tagih + int.parse(data['total_porsi'].toString());
      this.uang_bayar = int.parse(data['bayar'].toString());
      this.kembalian = int.parse(data['kembalian'].toString());
      this.total_porsi = int.parse(data['total_porsi'].toString());
      this.paid = data['paid'].toString();

      update();
    }

    this.getListPesananLain();
    this.getListPorsiOrg();
  }

  void getListPesananLain() async{
    String no_nota = KasirModel.no_nota;
    var request = await http
        .get(Api.route[KasirModel.modules]['getListPesananLain'](no_nota));

    if (request.statusCode == 200) {
      int total_tagih = 0;
      var data = json.decode(request.body);
      List<Produk_transaksi_lain> dataAdd = [];
      for (var item in data['data']) {
        item["id"] = item['id'].toString();
        item["harga"] = item['harga'].toString();
        item["qty"] = item['qty'].toString();
        item["total_harga"] = item['total_harga'].toString();
        total_tagih += (int.parse(item['total_harga'].toString()));
        dataAdd.add(Produk_transaksi_lain.fromJson(item));
      }


//      print("TOTAL TAGIHAN ${total_tagih.toString()}");
      TransaksiModels.data_lain.clear();
      TransaksiModels.data_lain.addAll(dataAdd);
      this.reload_tagihan = false;
      this.total_tagihan += total_tagih;
      update();
    }
  }

  void getListPorsiOrg() async{
    String no_nota = KasirModel.no_nota;
    var request = await http
        .get(Api.route[KasirModel.modules]['getListPorsiOrg'](no_nota));

    print("DATa ${request.body.toString()}");
    if (request.statusCode == 200) {
      var data = json.decode(request.body);
      List<Porsi> dataAdd = [];
      for (var item in data['data']) {
        dataAdd.add(Porsi.fromJson(item));
      }

      TransaksiModels.data_porsi.clear();
      TransaksiModels.data_porsi.addAll(dataAdd);
      this.reload_tagihan = false;
      update();
    }
  }

  Future<String> addPesanan() async {
    String message = "";
    Map<String, dynamic> params = {};
    params["no_nota"] = KasirModel.no_nota;
    params["menu"] = TagihanModels.menu;
    params["harga"] = TagihanModels.harga;
    params["qty"] = TagihanModels.jumlah;


    var request = await http.post(
        Api.route[KasirModel.modules]['addPesanan'],
        body: params);
    if (request.statusCode == 200) {
      var data = json.decode(request.body);
      if(data["is_valid"]){
//        Get.snackbar("Informasi", data["message"].toString());
        message = "success";
      }
    }

    return message;
  }

  void generateTransaksi() async{
    this.reload = true;
    update();

    var request = await http.get(
        Api.route[KasirModel.modules]['generateTransaksi']);
    if (request.statusCode == 200) {
      var data = json.decode(request.body);
      if(data["is_valid"]){
        Get.snackbar("Informasi", data["message"].toString());
//        this.reload = false;
//        update();
        this.getListAntrianTagihan();
      }
    }
  }

  void hitungTotalHargaMenu() {
    int jumlah = TagihanModels.jumlah == "" ? 0 : int.parse(TagihanModels.jumlah);
    int harga = TagihanModels.harga == "" ? 0 : int.parse(TagihanModels.harga);
    this.total_harga_menu_lain = jumlah * harga;
    update();
//    print("JUMLAH ${jumlah.toString()} dan HARGA ${harga.toString()}");

  }

  void deleteTransaksiItem(String id) async{
    Map<String, dynamic> params = {};
    params["id"] = id;

    this.reload_tagihan = true;
    update();
    var request = await http.post(
        Api.route[KasirModel.modules]['deleteTransaksiItem'],
        body: params);

    if (request.statusCode == 200) {
      var data = json.decode(request.body);
      if(data["is_valid"]){
        Get.snackbar("Informasi", "Berhasil Dihapus");
        this.reload_tagihan = false;
        update();
        this.getListPesanan();
      }
    }
  }

  void deleteTransaksiItemLain(String id) async{
    Map<String, dynamic> params = {};
    params["id"] = id;

    this.reload_tagihan = true;
    update();
    var request = await http.post(
        Api.route[KasirModel.modules]['deleteTransaksiItemLain'],
        body: params);

    if (request.statusCode == 200) {
      var data = json.decode(request.body);
      if(data["is_valid"]){
        Get.snackbar("Informasi", "Berhasil Dihapus");
        this.reload_tagihan = false;
        update();
        this.getListPesanan();
      }
    }
  }

  Future<String> editJumlahOrgMakan() async{
    Map<String, dynamic> params = {};
    params["no_nota"] = KasirModel.no_nota;
    params["qty_org"] = KasirModel.qty_org;

    var request = await http.post(
        Api.route[KasirModel.modules]['editJumlahOrgMakan'],
        body: params);

    String message = "";
//    print("request.body ${request.body.toString()}");
    if (request.statusCode == 200) {
      var data = json.decode(request.body);
      if(data["is_valid"]){
//        Get.snackbar("Informasi", "Berhasil Dirubah");
        message = "success";
      }
    }

    return message;
  }


  void cariBluetoothPrinter(PrinterBluetoothManager printerManager) async{
    this.scanning = true;
    update();

//    print("SCANNE BRO ${this.scanning.toString()}");
    printerManager.startScan(Duration(seconds: 4));
    printerManager.scanResults.listen((scannedDevices) {
      print("Sedang Mencari Bluetooth");
      this.devices=scannedDevices;
      this.scanning = false;
      update();
    }).onDone(() {

    });
  }


}