
import 'dart:convert';

import 'package:get/get.dart';
import 'package:http/http.dart' as http;
import 'package:wponline/app/models/kasir.models.dart';
import 'package:wponline/app/models/tagihan.models.dart';
import 'package:wponline/app/storage/produk_transaksi.dart';
import 'package:wponline/routes/api.dart';

class AntrianController extends GetxController{
  bool reload = true;

  void getListAntrianTagihan() async{
    var request = await http
        .get(Api.route[KasirModel.modules]['getListAntrianTagihan']);

    if (request.statusCode == 200) {
      var data = json.decode(request.body);
      List<Produk_transaksi> dataAdd = [];
      for (var item in data['data']) {
        item["id"] = item['id'].toString();
        dataAdd.add(Produk_transaksi.fromJson(item));
      }

      TagihanModels.data.clear();
      TagihanModels.data.addAll(dataAdd);
      this.reload = false;
      update();
    }
  }


  void deleteTransaksi(String no_transaksi) async{
    Map<String, dynamic> params = {};
    params["no_nota"] = no_transaksi;
    this.reload = true;
    update();

    var request = await http.post(
        Api.route[KasirModel.modules]['deleteTransaksi'],
        body: params);
//    print("DATA HASIL ${request.body.toString()}");
    if (request.statusCode == 200) {
      var data = json.decode(request.body);
      if(data["is_valid"]){
        Get.snackbar("Informasi", "Berhasil Dihapus");
        this.getListAntrianTagihan();
      }
    }
  }

}