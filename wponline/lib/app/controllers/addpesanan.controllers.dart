
import 'package:get/get.dart';
import 'package:wponline/app/models/tagihan.models.dart';

class AddPesananController extends GetxController{
  int total_harga_menu_lain = 0;
  bool calculate = false;


  void hitungTotalHargaMenu() {
    this.calculate = true;
    update();

    int jumlah = TagihanModels.jumlah == "" ? 0 : int.parse(TagihanModels.jumlah);
    int harga = TagihanModels.harga == "" ? 0 : int.parse(TagihanModels.harga);

    this.total_harga_menu_lain = jumlah * harga;

//    print("HARga TOTAL ${this.total_harga_menu_lain.toString()}");
    update();
  }
}