
import 'package:esc_pos_printer/esc_pos_printer.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class PrintDapurController extends GetxController{
  bool scanning = false;
  List<PrinterBluetooth> devices = [];


  void cariBluetoothPrinter(PrinterBluetoothManager printerManager) async{
    this.scanning = true;
    update();

    print("SCANNE BRO ${this.scanning.toString()}");
    printerManager.startScan(Duration(seconds: 4));
    printerManager.scanResults.listen((scannedDevices) {
//          print("SCANENED DEVICE ${scannedDevices.toString()}");
//          if(scannedDevices.length == 0){
//            Get.snackbar("Device", "Sedang Mencari Bluetooth", colorText: Colors.white, backgroundColor: Colors.orange);
//          }
        print("Sedang Mencari Bluetooth");
        this.devices=scannedDevices;
        this.scanning = false;
        update();
    }).onDone(() {

    });
  }
}