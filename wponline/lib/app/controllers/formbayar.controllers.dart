
import 'dart:convert';

import 'package:get/get.dart';
import 'package:wponline/app/models/kasir.models.dart';
import 'package:wponline/app/models/tagihan.models.dart';
import 'package:http/http.dart' as http;
import 'package:wponline/routes/api.dart';

class FormBayarController extends GetxController{

  Future<String> prosesBayar(int total) async{
    String message = "";
    if(int.parse(TagihanModels.uang_bayar) < total){
      Get.snackbar("Informasi", "Uang Pembayaran Tidak Cukup");
    }else{
      int kemblian = int.parse(TagihanModels.uang_bayar) - total;
      Map<String, dynamic> params = {};
      params['no_nota'] = KasirModel.no_nota;
      params['uang_bayar'] = TagihanModels.uang_bayar;
      params['kembalian'] = kemblian.toString();

      var request = await http.post(
      Api.route[KasirModel.modules]['prosesBayar'],
      body: params);
      if (request.statusCode == 200) {
        var data = json.decode(request.body);
        if(data["is_valid"]){
//          Get.snackbar("Informasi", data["message"].toString());
          message = "success";
        }
      }
    }

    return message;
  }
}