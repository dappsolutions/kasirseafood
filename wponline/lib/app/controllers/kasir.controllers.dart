

import 'dart:convert';

import 'package:get/get.dart';
import 'package:http/http.dart' as http;
import 'package:wponline/app/models/kasir.models.dart';
import 'package:wponline/app/models/menu.models.dart';
import 'package:wponline/app/models/produk.models.dart';
import 'package:wponline/app/models/transaksi.models.dart';
import 'package:wponline/app/storage/menu.dart';
import 'package:wponline/app/storage/produk.dart';
import 'package:wponline/app/storage/produk_has_stok.dart';
import 'package:wponline/app/storage/produk_transaksi_detail.dart';
import 'package:wponline/resources/views/kasir/detail.pesanan.kasir.views.dart';
import 'package:wponline/resources/views/tagihan/form.add.pesanan.views.dart';
import 'package:wponline/routes/api.dart';

class KasirController extends GetxController{
    int total_tagihan = 0;
    void generateNoTransaksi() async{
      var request = await http
          .get(Api.route[KasirModel.modules]['generateNoTransaksi']);
      if (request.statusCode == 200) {
        KasirModel.no_nota = request.body.toString();
        update();
      }
    }

    void simpan() async{
      KasirModel.params["no_nota"] = KasirModel.no_nota;
      KasirModel.params["no_tambahan"] = KasirModel.no_tambahan;
      KasirModel.params["menu"] = KasirModel.default_tipe_menu;
      KasirModel.params["produk"] = KasirModel.default_tipe_produk;

      var request = await http.post(
          Api.route[KasirModel.modules]['simpan'],
          body: KasirModel.params);

      print("DATA SIMAPN ${request.body.toString()}");
      if (request.statusCode == 200) {
        var data = json.decode(request.body);
        if(data["is_valid"]){
          KasirModel.total_item = data["total_item"].toString();
          Get.snackbar("Informasi", data["message"].toString());
          this.getListProduk();
          update();
        }
      }
    }

    void selesaiProses(bool gotoDapur, bool addingmenu) async{

      if(!addingmenu){
        KasirModel.params["no_nota"] = KasirModel.no_nota;
        var request = await http.post(
            Api.route[KasirModel.modules]['selesaiProses'],
            body: KasirModel.params);
        if (request.statusCode == 200) {
          var data = json.decode(request.body);
          if(data["is_valid"]){
            Get.snackbar("Informasi", data["message"].toString());
            if(gotoDapur){
              Get.to(DetailPesananKasirView(addingMenu: addingmenu,));
            }
          }
        }
      }else{
        Get.to(DetailPesananKasirView(addingMenu: addingmenu,));
      }
    }

//    void getListProduk() async{
//      var request = await http
//          .get(Api.route[ProdukModel.modules]['getListProduk']);
//
//      if (request.statusCode == 200) {
//        var data = json.decode(request.body);
//        List<Produk> dataAdd = [];
//        for (var item in data['data']) {
//          item["id"] = item['id'].toString();
//          dataAdd.add(Produk.fromJson(item));
//        }
//
//        if(dataAdd.length > 0){
//            KasirModel.default_tipe_produk = dataAdd[0].id.toString();
//        }
//        ProdukModel.data.clear();
//        ProdukModel.data.addAll(dataAdd);
//        update();
//      }
//    }
    void getListProduk() async{
      var request = await http
          .get(Api.route[ProdukModel.modules]['getListStok']);

      KasirModel.default_tipe_produk = "";
      update();
      if (request.statusCode == 200) {
        var data = json.decode(request.body);
        List<Produk_has_stok> dataAdd = [];
        for (var item in data['data']) {
          item["id"] = item['id'].toString();
          dataAdd.add(Produk_has_stok.fromJson(item));
        }

        if(dataAdd.length > 0){
            KasirModel.default_tipe_produk = dataAdd[0].id.toString();
        }
        ProdukModel.data_stok.clear();
        ProdukModel.data_stok.addAll(dataAdd);
        update();
      }
    }

    void updateTipeProduk(String params){
        KasirModel.default_tipe_produk = params;
        update();
    }

    void getListMenu() async{
      var request = await http
          .get(Api.route[MenuModel.modules]['getListMenu']);

      if (request.statusCode == 200) {
        var data = json.decode(request.body);
        List<Menu> dataAdd = [];
        for (var item in data['data']) {
          item["id"] = item['id'].toString();
          dataAdd.add(Menu.fromJson(item));
        }

        if(dataAdd.length > 0){
            KasirModel.default_tipe_menu = dataAdd[0].id.toString();
        }
        MenuModel.data.clear();
        MenuModel.data.addAll(dataAdd);
        update();
      }
    }

    void updateMenu(String params){
        KasirModel.default_tipe_menu = params;
        update();
    }

    void clearData(){
      KasirModel.total_item = "0";
      update();
    }

    void clearForm(){
      KasirModel.jml_ikan = "";
      KasirModel.berat_ikan = "";
      update();
    }

    void getListPesanan() async{
      String no_nota = KasirModel.no_nota;
      var request = await http
          .get(Api.route[KasirModel.modules]['getListPesanan'](no_nota));

      if (request.statusCode == 200) {
        int total_pesan = 0;
        var data = json.decode(request.body);
        List<Produk_transaksi_detail> dataAdd = [];
        for (var item in data['data']) {
          item["id"] = item['id'].toString();
          item["qty_org"] = item['qty_org'].toString();
          item["qty_produk"] = item['qty_produk'].toString();
          item["berat_produk"] = item['berat_produk'].toString();
          item["total_harga"] = item['total_harga'].toString();
          total_pesan += int.parse(item['total_harga'].toString());
          dataAdd.add(Produk_transaksi_detail.fromJson(item));
        }

        TransaksiModels.data.clear();
        TransaksiModels.data.addAll(dataAdd);
        TransaksiModels.reload = false;
        this.total_tagihan = total_pesan;
        update();
      }
    }

  void updateReloadTrans(bool reload) {
      TransaksiModels.reload = reload;
      update();
  }

  void generateNoTambahan() async{
    var request = await http
        .get(Api.route[KasirModel.modules]['generateNoTambahan'](KasirModel.no_nota));
    if (request.statusCode == 200) {
      KasirModel.no_tambahan = request.body.toString();
      update();
    }
  }
}