

import 'package:get/get.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

import 'package:wponline/app/models/produk.models.dart';
import 'package:wponline/app/storage/produk.dart';
import 'package:wponline/app/storage/produk_has_stok.dart';
import 'package:wponline/routes/api.dart';

class TipeIkanController extends GetxController{
  bool reload = true;

  void getListProduk() async{
    var request = await http
        .get(Api.route[ProdukModel.modules]['getListProduk']);

    if (request.statusCode == 200) {
      var data = json.decode(request.body);
      List<Produk> dataAdd = [];
      for (var item in data['data']) {
        item["id"] = item['id'].toString();
        dataAdd.add(Produk.fromJson(item));
      }

      ProdukModel.data.clear();
      ProdukModel.data.addAll(dataAdd);
      this.reload = false;
      update();
    }
  }

  void getListStok() async{
    var request = await http
        .get(Api.route[ProdukModel.modules]['getListStok']);

    if (request.statusCode == 200) {
      var data = json.decode(request.body);
      List<Produk_has_stok> dataAdd = [];
      for (var item in data['data']) {
        item["id"] = item['id'].toString();
        dataAdd.add(Produk_has_stok.fromJson(item));
      }

      ProdukModel.data_stok.clear();
      ProdukModel.data_stok.addAll(dataAdd);
      this.reload = false;
      update();
    }
  }

  void simpan() async{
    Map<String, dynamic> params = {};
    params["jenis"] = ProdukModel.jenis_produk;
    params["harga"] = ProdukModel.harga_produk;

    var request = await http.post(
        Api.route[ProdukModel.modules]['simpan'],
        body: params);
    if (request.statusCode == 200) {
      var data = json.decode(request.body);
      if(data["is_valid"]){
        Get.snackbar("Informasi", "Berhasil disimpan");
        this.reload = true;
        update();
        this.getListStok();
      }
    }
  }

  Future<String> simpanStok(String id_ikan) async{
    Map<String, dynamic> params = {};
    params["id"] = id_ikan;
    params["stok"] = ProdukModel.jumlah_stok;
    params["supplier"] = ProdukModel.supplier;

    var request = await http.post(
        Api.route[ProdukModel.modules]['simpanStok'],
        body: params);
    String message = "";
    if (request.statusCode == 200) {
      var data = json.decode(request.body);
      if(data["is_valid"]){
        message = "success";
      }
    }

    return message;
  }

  void deleteData(String id) async{
    Map<String, dynamic> params = {};
    params["id"] = id;

    this.reload = true;
    update();
    var request = await http.post(
        Api.route[ProdukModel.modules]['deleteData'],
        body: params);
//    print("HAPUS "+request.body.toString());
    if (request.statusCode == 200) {
      var data = json.decode(request.body);
      if(data["is_valid"]){
        Get.snackbar("Informasi", "Data Berhasil Dihapus");
        this.getListStok();
      }
    }
  }
}