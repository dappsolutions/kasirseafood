

import 'package:get/get.dart';
import 'package:wponline/app/models/login.models.dart';
import 'package:http/http.dart' as http;
import 'package:wponline/resources/views/dashboard/main.dashboard.views.dart';
import 'dart:convert';

import 'package:wponline/routes/api.dart';

class LoginController extends GetxController{
  bool reload = false;
  void login() async{
    this.reload = true;
    update();

    Map<String, dynamic> params = {};
    params["username"] = LoginModel.username_input;
    params["password"] = LoginModel.password_input;

    var request = await http.post(
        Api.route[LoginModel.modules]['sign_in'],
        body: params);
    if (request.statusCode == 200) {
      var data = json.decode(request.body);
      if(data["is_valid"]){
        Get.snackbar("Informasi", "Berhasil Masuk");
        LoginModel.user_id = data["user_id"].toString();
        LoginModel.hak_akses = data["hak_akses"];
        this.reload = false;
        update();

        Get.to(MainDashboardView());
      }else{
        Get.snackbar("Informasi", "Username dan password tidak ditemukan");
        this.reload = false;
        update();
      }
    }
  }

}