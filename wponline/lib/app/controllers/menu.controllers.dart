
import 'package:get/get.dart';
import 'package:http/http.dart' as http;
import 'package:wponline/app/models/menu.models.dart';
import 'package:wponline/app/storage/menu.dart';
import 'dart:convert';

import 'package:wponline/routes/api.dart';

class MenuController extends GetxController{
  bool reload = true;


  void getListMenu() async{
    var request = await http
        .get(Api.route[MenuModel.modules]['getListMenu']);

    if (request.statusCode == 200) {
      var data = json.decode(request.body);
      List<Menu> dataAdd = [];
      for (var item in data['data']) {
        item["id"] = item['id'].toString();
        dataAdd.add(Menu.fromJson(item));
      }

      MenuModel.data.clear();
      MenuModel.data.addAll(dataAdd);
      this.reload = false;
      update();
    }
  }

  void simpan() async{
    Map<String, dynamic> params = {};
    params["jenis"] = MenuModel.jenis_menu;

    var request = await http.post(
        Api.route[MenuModel.modules]['simpan'],
        body: params);
    if (request.statusCode == 200) {
      var data = json.decode(request.body);
      if(data["is_valid"]){
        Get.snackbar("Informasi", "Berhasil disimpan");
        this.reload = true;
        update();
        this.getListMenu();
      }
    }
  }

  void deleteData(String id) async{
    Map<String, dynamic> params = {};
    params["id"] = id;

    this.reload = true;
    update();
    var request = await http.post(
        Api.route[MenuModel.modules]['deleteData'],
        body: params);
//    print("HAPUS "+request.body.toString());
    if (request.statusCode == 200) {
      var data = json.decode(request.body);
      if(data["is_valid"]){
        Get.snackbar("Informasi", "Data Berhasil Dihapus");
        this.getListMenu();
      }
    }
  }
}