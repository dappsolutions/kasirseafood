
import 'dart:convert';

import 'package:get/get.dart';
import 'package:wponline/app/models/kasir.models.dart';
import 'package:http/http.dart' as http;
import 'package:wponline/app/models/transaksi.models.dart';
import 'package:wponline/app/storage/produk_transaksi_detail.dart';
import 'package:wponline/routes/api.dart';

class TransaksiController extends GetxController{
  int total_tagihan = 0;

  void getListPesanan(bool addingMenu) async{
    String no_nota = KasirModel.no_nota;
    String no_tambahan = KasirModel.no_tambahan;

    var request = !addingMenu ? await http
        .get(Api.route[KasirModel.modules]['getListPesanan'](no_nota)) : await http
        .get(Api.route[KasirModel.modules]['getListPesananByNoTambahan'](no_nota, no_tambahan));

    if (request.statusCode == 200) {
      int total_pesan = 0;
      var data = json.decode(request.body);
      List<Produk_transaksi_detail> dataAdd = [];
      for (var item in data['data']) {
        item["id"] = item['id'].toString();
        item["qty_org"] = item['qty_org'].toString();
        item["qty_produk"] = item['qty_produk'].toString();
        item["berat_produk"] = item['berat_produk'].toString();
        item["total_harga"] = item['total_harga'].toString();
        total_pesan += int.parse(item['total_harga'].toString());
        dataAdd.add(Produk_transaksi_detail.fromJson(item));
      }

      TransaksiModels.data.clear();
      TransaksiModels.data.addAll(dataAdd);
      TransaksiModels.reload = false;
      this.total_tagihan = total_pesan;
      update();
    }
  }

  void updateReloadTrans(bool reload) {
    TransaksiModels.reload = reload;
    update();
  }

  void deleteTransaksiItem(id, bool addingMenu) async{
    Map<String, dynamic> params = {};
    params["id"] = id;

//    print("ID ${id.toString()}, dan addmenu ${addingMenu.toString()}");
    var request = await http.post(
        Api.route[KasirModel.modules]['deleteTransaksiItem'],
        body: params);
//    print("DATA HASIL ${request.body.toString()}");
    if (request.statusCode == 200) {
      var data = json.decode(request.body);
      if(data["is_valid"]){
        Get.snackbar("Informasi", "Berhasil Dihapus");
        this.getListPesanan(addingMenu);
      }
    }
  }
}