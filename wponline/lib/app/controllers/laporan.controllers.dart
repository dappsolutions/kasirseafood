
import 'dart:convert';

import 'package:get/get.dart';
import 'package:http/http.dart' as http;
import 'package:wponline/app/models/kasir.models.dart';
import 'package:wponline/app/models/laporan.models.dart';
import 'package:wponline/app/models/produk.models.dart';
import 'package:wponline/app/storage/produk_stok.dart';
import 'package:wponline/app/storage/produk_tambah_stok.dart';
import 'package:wponline/app/storage/produk_transaksi_summary.dart';
import 'package:wponline/routes/api.dart';

class LaporanController extends GetxController{
  bool reload = true;
  String total_penghasilan = "0";
  String total_terjual = "0";

  void getListLaporan() async{
    var request = await http
        .get(Api.route[KasirModel.modules]['getListLaporan']);

    if (request.statusCode == 200) {
      var data = json.decode(request.body);
      List<Produk_transaksi_summary> dataAdd = [];
      for (var item in data['data']) {
        item["id"] = item['id'].toString();
        item["total"] = item['total'].toString();
        dataAdd.add(Produk_transaksi_summary.fromJson(item));
      }

      this.total_penghasilan = data["total"].toString();

      LaporanModel.data.clear();
      LaporanModel.data.addAll(dataAdd);
      this.reload = false;
      update();
    }
  }

  void deleteLaporan(String no_transaksi) async{
      Map<String, dynamic> params = {};
      params["no_nota"] = no_transaksi;

      this.reload = true;
      update();

      var request = await http.post(
          Api.route[KasirModel.modules]['deleteLaporan'],
          body: params);
      if (request.statusCode == 200) {
        var data = json.decode(request.body);
        if(data["is_valid"]){
          Get.snackbar("Informasi", "Berhasil Dihapus");
          this.getListLaporan();
        }
      }
  }


  void getLaporanStok() async{
    var request = await http
        .get(Api.route[KasirModel.modules]['getLaporanStok']);

    if (request.statusCode == 200) {
      var data = json.decode(request.body);
      List<Produk_stok> dataAdd = [];
      for (var item in data['data']) {
        dataAdd.add(Produk_stok.fromJson(item));
      }

      this.total_terjual = data["total_all"].toString();

      LaporanModel.data_stok.clear();
      LaporanModel.data_stok.addAll(dataAdd);
      this.reload = false;
      update();
    }
  }

  void getLaporanPenambahanStok() async {
    var request = await http
        .get(Api.route[ProdukModel.modules]['getLaporanPenambahanStok']);

    if (request.statusCode == 200) {
      var data = json.decode(request.body);
      List<Produk_tambah_stok> dataAdd = [];
      for (var item in data['data']) {
        dataAdd.add(Produk_tambah_stok.fromJson(item));
      }

      ProdukModel.data_stok_tambahan.clear();
      ProdukModel.data_stok_tambahan.addAll(dataAdd);
      this.reload = false;
      update();
    }
  }

  void deleteLaporanStok(String id) async{
    Map<String, dynamic> params = {};
    params["id"] = id;

    this.reload = true;
    update();

    var request = await http.post(
        Api.route[ProdukModel.modules]['deleteLaporanStok'],
        body: params);
    if (request.statusCode == 200) {
      var data = json.decode(request.body);
      if(data["is_valid"]){
        Get.snackbar("Informasi", "Berhasil Dihapus");
        this.getLaporanPenambahanStok();
      }
    }
  }

  void deleteAllLaporan() async{
//    print("HAPUS SEMUA");
    var request = await http
        .get(Api.route[KasirModel.modules]['deleteAllLaporan']);

    this.reload = true;
    update();
    if (request.statusCode == 200) {
      var data = json.decode(request.body);
      this.getListLaporan();
    }
  }

  void getListLaporanBulanan(String bulan, String tahun) async{
    Map<String, dynamic> params = {};
    params["bulan"] = bulan;
    params["tahun"] = tahun;

    var request = await http
        .post(Api.route[KasirModel.modules]['getListLaporanBulanan'], body: params);

    print("DATa REQUEST ${request.body.toString()}");
    if (request.statusCode == 200) {
      print("DATA BULAN ${request.body.toString()}");
      var data = json.decode(request.body);
      List<Produk_transaksi_summary> dataAdd = [];
      for (var item in data['data']) {
        item["id"] = item['id'].toString();
        item["total"] = item['total'].toString();
        dataAdd.add(Produk_transaksi_summary.fromJson(item));
      }

      this.total_penghasilan = data["total"].toString();

      LaporanModel.data.clear();
      LaporanModel.data.addAll(dataAdd);
      this.reload = false;
      update();
    }
  }
}