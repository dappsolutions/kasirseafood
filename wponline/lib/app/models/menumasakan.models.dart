
import 'package:wponline/app/storage/menu.dart';

class MenuMasakanModels{
  static List<Menu> data_trans = [
    Menu(
      id: "1",
      jenis: "BAKAR",
    ),
    Menu(
      id: "2",
      jenis: "GOREN BIASA",
    ),
    Menu(
      id: "3",
      jenis: "KRISPI",
    ),
  ];
}