

import 'package:wponline/app/controllers/kasir.controllers.dart';
import 'package:wponline/app/storage/produk_transaksi_detail.dart';

class KasirModel{
  static String no_nota = "";
  static String modules = "pembelian";
  static Map<String, dynamic> params = {};
  static String default_tipe_produk = "";
  static String default_tipe_menu = "";
  static String total_item = "0";
  static String jml_ikan = "";
  static String qty_org = "";
  static String berat_ikan = "";
  static String keterangan = "";
  static String no_antrian = "";
  static String no_tambahan = "";
  static List<Produk_transaksi_detail> detail_trans = [
    Produk_transaksi_detail(
      id: "1",
      qty_org: "3",
      qty_produk: "1",
      jenis_produk: "IKAN BAKAR",
      berat_produk: "1 Kg",
      total_harga: "80.000"
    ),
    Produk_transaksi_detail(
      id: "2",
      qty_org: "3",
      qty_produk: "1",
      jenis_produk: "CUMI GORENG BIASA",
      berat_produk: "1 Kg",
      total_harga: "80.000"
    ),
    Produk_transaksi_detail(
      id: "3",
      qty_org: "3",
      qty_produk: "1",
      jenis_produk: "UDANG GORENG BIASA",
      berat_produk: "1 Kg",
      total_harga: "80.000"
    ),
  ];
}