

import 'package:wponline/app/storage/produk_stok.dart';
import 'package:wponline/app/storage/produk_transaksi_summary.dart';

class LaporanModel{
  static List<Produk_transaksi_summary> data = [];
  static List<Produk_stok> data_stok = [];
  static List<String> data_tahun = ["2020", "2021", "2022"];
  static List<String> data_bulan = [
    "Januari",
    "Februari",
    "Maret",
    "April",
    "Mei",
    "Juni",
    "Juli",
    "Agustus",
    "September",
    "Oktober",
    "November",
    "Desember"
  ];
}