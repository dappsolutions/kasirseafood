
import 'package:wponline/app/storage/produk.dart';
import 'package:wponline/app/storage/produk_has_stok.dart';
import 'package:wponline/app/storage/produk_tambah_stok.dart';

class ProdukModel{
  static String modules = "produk";
  static List<Produk> data = [];
  static List<Produk_has_stok> data_stok = [];
  static List<Produk_tambah_stok> data_stok_tambahan = [];
  static String jenis_produk = "";
  static String harga_produk = "";
  static String jumlah_stok = "";
  static String supplier = "";
  static String stok_awal = "";
  static String stok_oustanding = "";
}