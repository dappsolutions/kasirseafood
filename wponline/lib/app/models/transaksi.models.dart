import 'package:wponline/app/storage/porsi.dart';
import 'package:wponline/app/storage/produk_transaksi_detail.dart';
import 'package:wponline/app/storage/produk_transaksi_lain.php.dart';

class TransaksiModels{
  static bool reload = false;
  static List<Produk_transaksi_detail> data = [];
  static List<Porsi> data_porsi = [];
  static List<Produk_transaksi_lain> data_lain = [];
}